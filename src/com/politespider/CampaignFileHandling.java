package com.politespider;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.XMLEvent;

public class CampaignFileHandling {
	
	public static CampaignInfo loadCampaignFile (String filename) {
		XMLEventReader eventReader = null;
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		InputStream in = null;
		try {
			in = new FileInputStream(filename);
		} catch (Exception e) {
			System.out.println("file not loaded");
		}
		try {
			eventReader = inputFactory.createXMLEventReader(in);
		} catch (Exception e) {	
			System.out.println("event reader error");
		}
		CampaignInfo result = new CampaignInfo();
		// Now its been loaded, its time to read the XML document
		while (eventReader.hasNext()) {
			XMLEvent event = null;
			try {
				event = eventReader.nextEvent();
			} catch (Exception e) {
				System.out.println("eventReader.nextEvent error");
			}
			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart() == ("X3SGMversion")) {
					try {
						event = eventReader.nextEvent();
					} catch (Exception e) {
						System.out.println("problem in X3SGMversion event");
					}
					result.setX3SGMversion(Double.valueOf(event.toString()));
					if (result.getX3SGMversion()==1.0) {
						result.setLogEntryNumber(1);
						result.setLogEntryTemplate(Globals.defaultLogEntry);
					}
				}
			}
			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart() == ("campaignName")) {
					try {
						event = eventReader.nextEvent();
					} catch (Exception e) {
						System.out.println("problem in campaignName event");
					}
					result.setCampaignName(event.toString());
				}
			}		
			if (event.isStartElement()) {// deprecated in new campaign info files, but still needed in order to load older campaign info files
				if (event.asStartElement().getName().getLocalPart() == ("comment")) {
					try {
						event = eventReader.nextEvent();
					} catch (Exception e) {
						System.out.println("problem in comment event");
					}
					result.setLogFile(event.toString());
				}
			}
			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart() == ("logFile")) {
					try {
						event = eventReader.nextEvent();
					} catch (Exception e) {
						System.out.println("problem in logFile event");
					}
					result.setLogFile(event.toString());
				}
			}
			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart() == ("logEntryTemplate")) {
					try {
						event = eventReader.nextEvent();
					} catch (Exception e) {
						System.out.println("problem in logEntryTemplate event");
					}
					result.setLogEntryTemplate(event.toString());
				}
			}
			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart() == ("logEntryNumber")) {
					try {
						event = eventReader.nextEvent();
					} catch (Exception e) {
						System.out.println("problem in logEntryNumber event");
					}
					result.setLogEntryNumber(Integer.decode(event.toString()));
				}
			}
		}
		return result;
	}

	public static void newCampaignFile ( String destinationFile,String title, String comment, int logEntryNumber, String logEntryTemplate) {
		BufferedWriter xmlOut = null;
		File fName = new File(destinationFile);
		try {
			xmlOut = new BufferedWriter(new FileWriter(fName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("<root xsi:noNamespaceSchemaLocation = \"X3SaveGameManager.xsd\" xmlns:xsi = \"http://www.w3.org/2001/XMLSchema-instance\">\n"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <X3SGMversion>"+Globals.campaignFileversion+"</X3SGMversion>\n"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <campaignName><![CDATA["+title+"]]></campaignName>\n"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <logEntryNumber>"+logEntryNumber+"</logEntryNumber>\n"));	
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <logEntryTemplate><![CDATA["+logEntryTemplate+"]]></logEntryTemplate>\n"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <logFile><![CDATA["+comment+"]]></logFile>\n"));		
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("</root>"));
			xmlOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void writeRevisedCampaignFile ( String destinationFile,CampaignInfo revised) {
		File fName = new File(destinationFile);
		BufferedWriter xmlOut = null;
		try {
			xmlOut = new BufferedWriter(new FileWriter(fName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("<root xsi:noNamespaceSchemaLocation = \"X3SaveGameManager.xsd\" xmlns:xsi = \"http://www.w3.org/2001/XMLSchema-instance\">\n"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <X3SGMversion>"+Globals.campaignFileversion+"</X3SGMversion>\n"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <campaignName><![CDATA["+revised.getCampaignName()+"]]></campaignName>\n"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <logEntryNumber>"+revised.getLogEntryNumber()+"</logEntryNumber>\n"));	
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <logEntryTemplate><![CDATA["+revised.getLogEntryTemplate()+"]]></logEntryTemplate>\n"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <logFile><![CDATA["+revised.getLogFile()+"]]></logFile>\n"));		
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("</root>"));
			xmlOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
