package com.politespider;

public class CampaignUtils {
	
	public static boolean isActiveCampaignAlreadyStored() {
		for (int f = 0; f<Globals.allCampaigns.size();f++) {
			if (Globals.activeCampaign.getCampaignName().equals(Globals.allCampaigns.get(f).getCampaignName())) {
				return true;
			}
		}
		return false;
	}

	public static String processLogFileTemplate(String template) {
		String unprocessed = new String(template);
		String intermediate = new String();
		// this isn't too smart just yet. It checks for the 
		// placeholders I have one at a time
		if (unprocessed.length()==0) {//empty template, just return it as is.
			return unprocessed;
		}
		// first, for the log number
		String lowercased = new String(unprocessed).toLowerCase();
		if (lowercased.indexOf(Globals.lognum) > -1) {
			// lognum is present, strip it out and replace it with the actual log number
			int start = lowercased.indexOf(Globals.lognum);
			for (int i = 0;i<start;i++) {
				intermediate = intermediate.concat(""+unprocessed.charAt(i));
			}
			intermediate = intermediate.concat(""+Globals.activeCampaign.getLogEntryNumber());
			for (int j = start+Globals.lognum.length();j<unprocessed.length();j++) {
				intermediate = intermediate.concat(""+unprocessed.charAt(j));
			}
		}   
		//and again for the date
		unprocessed = intermediate;
		lowercased = new String(unprocessed).toLowerCase();
		if (lowercased.indexOf(Globals.datetime) > -1) {
			intermediate = "";
			// datetime is present, strip it out and replace it with the actual date string
			int start = lowercased.indexOf(Globals.datetime);
			for (int i = 0;i<start;i++) {
				intermediate = intermediate.concat(""+unprocessed.charAt(i));
			}
			intermediate = intermediate.concat(""+DateMaker.getDateStamp());
			for (int j = start+Globals.datetime.length();j<unprocessed.length();j++) {
				intermediate = intermediate.concat(""+unprocessed.charAt(j));
			}
		} 
		return intermediate;
	}
}
