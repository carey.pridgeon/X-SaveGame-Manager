package com.politespider.X3.savegame.manager;
import com.cloudgarden.resource.SWTResourceManager;

import java.io.File;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;


public class DLG_ImportExportManager extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private Button importNewSavegameButton;
	private Button importCampaignButton;
	private StyledText impostJustSavegamesText;
	private Button exitButton;
	private ProgressBar progressBar2;
	private Label copyFileLabel;
	private Label exportCampaignLabel;
	private Label importCampaignLabel;
	private Button exportCampaignButton;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_ImportExportManager inst = new DLG_ImportExportManager(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_ImportExportManager(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			dialogShell.setLayout(new FormLayout());
			dialogShell.setText("Campaign Import and Export ");
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));
			{
				FormData impostJustSavegamesTextLData = new FormData();
				impostJustSavegamesTextLData.width = 187;
				impostJustSavegamesTextLData.height = 38;
				impostJustSavegamesTextLData.left =  new FormAttachment(0, 1000, 53);
				impostJustSavegamesTextLData.top =  new FormAttachment(0, 1000, 144);
				impostJustSavegamesText = new StyledText(dialogShell, SWT.READ_ONLY | SWT.WRAP);
				impostJustSavegamesText.setLayoutData(impostJustSavegamesTextLData);
				impostJustSavegamesText.setText("Import a new Savegame set into the Savegame Manager.");
				impostJustSavegamesText.setBackground(SWTResourceManager.getColor(255,238,219));
				impostJustSavegamesText.setEnabled(false);
			}
			{
				exitButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData exitButtonLData = new FormData();
				exitButtonLData.width = 168;
				exitButtonLData.height = 25;
				exitButtonLData.left =  new FormAttachment(0, 1000, 58);
				exitButtonLData.top =  new FormAttachment(0, 1000, 258);
				exitButton.setLayoutData(exitButtonLData);
				exitButton.setText("Close Import/Export Manager");
				exitButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) { 
						dialogShell.dispose();
					}

				});
			}
			{
				FormData progressBar1LData = new FormData();
				progressBar1LData.width = 279;
				progressBar1LData.height = 18;
				progressBar1LData.left =  new FormAttachment(0, 1000, 0);
				progressBar1LData.top =  new FormAttachment(0, 1000, 234);
				progressBar2 = new ProgressBar(dialogShell, SWT.NONE);
				progressBar2.setLayoutData(progressBar1LData);
			}
			{
				FormData copyFileLData = new FormData();
				copyFileLData.width = 265;
				copyFileLData.height = 15;
				copyFileLData.left =  new FormAttachment(0, 1000, 0);
				copyFileLData.top =  new FormAttachment(0, 1000, 213);
				copyFileLabel = new Label(dialogShell, SWT.NONE);
				copyFileLabel.setLayoutData(copyFileLData);
				copyFileLabel.setBackground(SWTResourceManager.getColor(255,238,219));
			}
			{
				importCampaignButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData importCampaignButtonLData = new FormData();
				importCampaignButtonLData.width = 173;
				importCampaignButtonLData.height = 25;
				importCampaignButtonLData.left =  new FormAttachment(0, 1000, 115);
				importCampaignButtonLData.top =  new FormAttachment(0, 1000, 33);
				FormData importCampaignButtonLData1 = new FormData();
				importCampaignButtonLData1.left =  new FormAttachment(0, 1000, 98);
				importCampaignButtonLData1.top =  new FormAttachment(0, 1000, 39);
				importCampaignButtonLData1.width = 100;
				importCampaignButtonLData1.height = 25;
				importCampaignButton.setLayoutData(importCampaignButtonLData1);
				importCampaignButton.setText("Import");
				importCampaignButton.setToolTipText("This allows you to import a campaign you have previously created into this instance of the savegame manager. This is useful for moving campaigns between machines");
				importCampaignButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) { 
						DirectoryDialog fileChooser3 = new DirectoryDialog(dialogShell, SWT.OPEN);
						fileChooser3.setText("Select the folder which contains the campaign you wish to import");
						String folderName = fileChooser3.open();
						if ((folderName == null)||(folderName =="")) {
							Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION |SWT.OK);
							Globals.mBox.setMessage("No import location specified");
							Globals.mBox.open();
							dialogShell.dispose();
						}	else {
							// scan the selected folder for the campaign info file
							File[] scan1 = FileHandling.listDirectoryContent(folderName,Globals.infoFileExtension,false);
							if (scan1.length ==0) {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION |SWT.OK);
								Globals.mBox.setMessage("No X3:TC Campaign file found in the specified folder:\n"+folderName+"\nIf this is a savegame set that has yet to be set up as a campaign, use the 'Import Savegame Set' button instead");
								Globals.mBox.open();
							}  else {
								// scan the selected folder for savegame files
								File[] scan2 = FileHandling.listDirectoryContent(folderName,Globals.saveFileExtension,false);
								if (scan2.length ==0) {
									Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("No X3:TC save files found in specified folder:\n"+folderName);
									Globals.mBox.open();
									System.exit(0);
								}	
								// we have a campaign file, and savegame files, so this is a campaign that can be imported as is.
								// so collect up a full files list
								File[] content = FileHandling.listDirectoryContent(folderName,Globals.campaignFileExtensions,false);
								if (content.length ==0) {
									Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("No X3:TC save files found in specified folder:\n"+folderName);
									Globals.mBox.open();
									System.exit(0);
								}
								Globals.importedCampaign = CampaignFileHandling.loadCampaignFile(folderName+"\\"+Globals.campaignFileName);
								// and check to make sure the selected campaign name isn't already in use
								if (FileHandling.isFilenameAlreadyInUse(Globals.importedCampaign.getCampaignName())) {
									Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_WARNING |SWT.YES|SWT.NO);
									Globals.mBox.setMessage("A campaign of that name already exists.\n Click 'YES' to replace it, 'NO' to abort.");
									int resp = Globals.mBox.open();
									if (resp == SWT.YES) {
										// Given that a local version of this campaign already exists,
										// check whether the campaign to be imported is currently the active campaign locally
										if (Globals.importedCampaign.getCampaignName().equals(Globals.activeCampaign.getCampaignName())) {
											Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_WARNING |SWT.YES|SWT.NO);
											Globals.mBox.setMessage("The campaign you are importing is currently also the active campaign. Continuing this operation will update the X3TC savegame folder content as well.\n If you don't want to do this, select 'NO', and this program will exit.");
											int resp2 = Globals.mBox.open();
											if (resp2 == SWT.NO) {
												System.exit(0);
											}
											// it is the active campaign
											FileHandling.emptyDirectory(Globals.saveFolder);
											progressBar2.setMinimum(0);
											progressBar2.setMaximum(content.length);
											for (int i=0;i<content.length;i++) {
												copyFileLabel.setText("Copying "+content[i].getName().toString());
												try {
													// copy to the storage folder and the X3TC savegame folder simultaniously
													FileHandling.copySingleFile(content[i].toString(),Globals.saveFolder+"/"+content[i].getName().toString());
													FileHandling.copySingleFile(content[i].toString(),Globals.campaignfolderNAME+Globals.importedCampaign.getCampaignName()+"/"+content[i].getName().toString());
												} catch (Exception e1) {
													Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
													Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+content[i].toString());
													Globals.mBox.open();
													System.exit(0);
												}
												progressBar2.setSelection(i);
											}
											progressBar2.setSelection(0);
											copyFileLabel.setText("Campaign '"+Globals.importedCampaign.getCampaignName()+"' imported succesfully.");
										} else {
											// it isn't the active campaign
											progressBar2.setMinimum(0);
											progressBar2.setMaximum(content.length);
											for (int i=0;i<content.length;i++) {
												copyFileLabel.setText("Copying "+content[i].getName().toString());
												try {
													FileHandling.copySingleFile(content[i].toString(),Globals.campaignfolderNAME+Globals.importedCampaign.getCampaignName()+"/"+content[i].getName().toString());
												} catch (Exception e1) {
													Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
													Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+content[i].toString());
													Globals.mBox.open();
													System.exit(0);
												}
												progressBar2.setSelection(i);
											}
											progressBar2.setSelection(0);
											copyFileLabel.setText("Campaign '"+Globals.importedCampaign.getCampaignName()+"' imported.");
										}
									} else {
										Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION |SWT.OK);
										Globals.mBox.setMessage("Aborted import campaign operation.");
										Globals.mBox.open();
									}
								} else { // campaign name isn't already in use
									// create the storage folder
									FileHandling.createFolder(Globals.campaignfolderNAME+Globals.importedCampaign.getCampaignName());
									progressBar2.setMinimum(0);
									progressBar2.setMaximum(content.length);
									for (int i=0;i<content.length;i++) {
										copyFileLabel.setText("Copying "+content[i].getName().toString());
										try {
											FileHandling.copySingleFile(content[i].toString(),Globals.campaignfolderNAME+Globals.importedCampaign.getCampaignName()+"/"+content[i].getName().toString());
										} catch (Exception e1) {
											Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
											Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+content[i].toString());
											Globals.mBox.open();
											System.exit(0);
										}
										progressBar2.setSelection(i);
									}
									progressBar2.setSelection(0);
									copyFileLabel.setText("Campaign '"+Globals.importedCampaign.getCampaignName()+"' imported succesfully.");
								}
							}
						}
					}
				});
			}
			{
				exportCampaignButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData exportCampaignButtonLData = new FormData();
				exportCampaignButtonLData.width = 173;
				exportCampaignButtonLData.height = 25;
				exportCampaignButtonLData.left =  new FormAttachment(0, 1000, 115);
				exportCampaignButtonLData.top =  new FormAttachment(0, 1000, 91);
				FormData exportCampaignButtonLData1 = new FormData();
				exportCampaignButtonLData1.left =  new FormAttachment(0, 1000, 98);
				exportCampaignButtonLData1.top =  new FormAttachment(0, 1000, 97);
				exportCampaignButtonLData1.width = 100;
				exportCampaignButtonLData1.height = 25;
				exportCampaignButton.setLayoutData(exportCampaignButtonLData1);
				exportCampaignButton.setText("Export");
				exportCampaignButton.setToolTipText("If you want to make external backups of a campaign, or transfer it to another machine, use this option ");
				exportCampaignButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) { 
						Globals.pickedCampaignIndex =-1;
						// set up the terxt for the dialog we are about to display
						Globals.campaignSelectBoxText = Globals.exportText;
						DLG_CampaignSelectBox cSBox = new DLG_CampaignSelectBox(dialogShell.getShell(),SWT.APPLICATION_MODAL);
						cSBox.open();
						if (Globals.pickedCampaignIndex ==-1){
							Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
							Globals.mBox.setMessage("No Campaign selected for export, aborting export operation");
							Globals.mBox.open();
							dialogShell.dispose();
						}	else {
							DirectoryDialog fileChooser3 = new DirectoryDialog(dialogShell, SWT.OPEN);
							fileChooser3.setText("Select the folder to export this campaign to contains the campaign you wish to import");
							String folderName = fileChooser3.open();
							if ((folderName == null)||(folderName =="")) {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION |SWT.OK);
								Globals.mBox.setMessage("No export location specified");
								Globals.mBox.open();
								dialogShell.dispose();
							}	else {
								// rather than just try and copy the whole folder, the operation
								// will be broken up into creating the folder and
								// transfering individual files

								// task 1: create the receiving folder
								try {
									FileHandling.createFolder(folderName+"\\"+Globals.allCampaigns.get(Globals.pickedCampaignIndex).getCampaignName());
								} catch (Exception e1) {
									Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("Unable to create folder to copy campaign to in the selected export folder - aborting");
									Globals.mBox.open();
									dialogShell.dispose();
								}
								// task 2: copy over the campaign files

								File[] content = FileHandling.listDirectoryContent(Globals.campaignfolderNAME+Globals.allCampaigns.get(Globals.pickedCampaignIndex).getCampaignName(),Globals.campaignFileExtensions,false);
								if (content.length ==0) {
									Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("No X3:TC save files found in specified folder:\n"+folderName);
									Globals.mBox.open();
									dialogShell.dispose();
								}
								progressBar2.setMinimum(0);
								progressBar2.setMaximum(content.length);
								for (int i=0;i<content.length;i++) {
									copyFileLabel.setText("Copying "+content[i].getName().toString());
									try {
										FileHandling.copySingleFile(content[i].toString(),folderName+"\\"+Globals.allCampaigns.get(Globals.pickedCampaignIndex).getCampaignName()+"\\"+content[i].getName().toString());
									} catch (Exception e1) {
										Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
										Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+content[i].toString());
										Globals.mBox.open();
										System.exit(0);
									}
									progressBar2.setSelection(i);
								}
								progressBar2.setSelection(0);
								copyFileLabel.setText("Campaign '"+Globals.allCampaigns.get(Globals.pickedCampaignIndex).getCampaignName()+"' exported succesfully.");	
							}
						}	
					}
				});
			}
			{
				importNewSavegameButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData importNewSavegameButtonLData = new FormData();
				importNewSavegameButtonLData.width = 173;
				importNewSavegameButtonLData.height = 25;
				importNewSavegameButtonLData.left =  new FormAttachment(0, 1000, 115);
				importNewSavegameButtonLData.top =  new FormAttachment(0, 1000, 182);
				FormData importNewSavegameButtonLData1 = new FormData();
				importNewSavegameButtonLData1.left =  new FormAttachment(0, 1000, 53);
				importNewSavegameButtonLData1.top =  new FormAttachment(0, 1000, 188);
				importNewSavegameButtonLData1.width = 173;
				importNewSavegameButtonLData1.height = 25;
				importNewSavegameButton.setLayoutData(importNewSavegameButtonLData1);
				importNewSavegameButton.setText("Import new Savegame Set");
				importNewSavegameButton.setToolTipText("If you have a set of savegames that you would like to convert into a campaign, import them with this option");
				importNewSavegameButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) { 
						DirectoryDialog fileChooser3 = new DirectoryDialog(dialogShell, SWT.OPEN);
						fileChooser3.setText("Select the folder which contains the savegames you wish to import");
						String folderName = fileChooser3.open();
						if ((folderName == null)||(folderName =="")) {
							Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION |SWT.OK);
							Globals.mBox.setMessage("No import location specified - ");
							Globals.mBox.open();
							dialogShell.dispose();
						}	else {
							// scan the selected folder for savegame files
							File[] content = FileHandling.listDirectoryContent(folderName,Globals.saveFileExtension,false);
							if (content.length ==0) {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
								Globals.mBox.setMessage("No X3:TC save files found in specified folder:\n"+folderName);
								Globals.mBox.open();
								System.exit(0);
							}
							// looks like we found some savegame files, so lets import those suckers.
							// null the fields we will need to be checking later to ensure data was entered
							Globals.campaignNameStorage = null;
							Globals.commentStorage = null;
							DLG_CampaignSetup setupBox = new DLG_CampaignSetup(dialogShell.getShell(),1);
							setupBox.open();
							
							//check to see that we have a campaign name specified, if not, exit (null or empty)
							if ((Globals.campaignNameStorage ==null)||(Globals.campaignNameStorage.length()==0)) {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
								Globals.mBox.setMessage("Campaign name not properly set - aborting operation");
								Globals.mBox.open();
							} else {
								// ok we got this far, the information to create a campaign file exists
								Globals.importedCampaign = new CampaignInfo();
								Globals.importedCampaign.setCampaignName(Globals.campaignNameStorage);
								if (Globals.commentStorage == null) {
									Globals.importedCampaign.setLogFile("");
								} else {
									Globals.importedCampaign.setLogFile(Globals.commentStorage);
								}
								// start off logfile entry numbering
								Globals.importedCampaign.setLogEntryNumber(Globals.startingLogNumber);
								// make the folder we are going to need
								FileHandling.createFolder(Globals.campaignfolderNAME+Globals.importedCampaign.getCampaignName());
								// write the campaign file to it
								CampaignFileHandling.newCampaignFile(Globals.campaignfolderNAME+Globals.importedCampaign.getCampaignName()+"\\"+Globals.campaignFileName, Globals.importedCampaign.getCampaignName(), Globals.importedCampaign.getLogFile(),Globals.importedCampaign.getLogEntryNumber(),Globals.defaultLogEntry);
								//now copy the files over
								progressBar2.setMinimum(0);
								progressBar2.setMaximum(content.length);
								for (int i=0;i<content.length;i++) {
									copyFileLabel.setText("Copying "+content[i].getName().toString());
									try {
										FileHandling.copySingleFile(content[i].toString(),Globals.campaignfolderNAME+Globals.importedCampaign.getCampaignName()+"/"+content[i].getName().toString());
									} catch (Exception e1) {
										Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
										Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+content[i].toString());
										Globals.mBox.open();
										System.exit(0);
									}
									progressBar2.setSelection(i);
								}
								progressBar2.setSelection(0);
								copyFileLabel.setText("Savegames imported succesfully.");
							}
						}
						
					}
				});
			}
			{
				importCampaignLabel = new Label(dialogShell, SWT.CENTER);
				FormData importCampaignLabelLData = new FormData();
				importCampaignLabelLData.width = 362;
				importCampaignLabelLData.height = 15;
				importCampaignLabelLData.left =  new FormAttachment(0, 1000, 22);
				importCampaignLabelLData.top =  new FormAttachment(0, 1000, 12);
				FormData importCampaignLabelLData1 = new FormData();
				importCampaignLabelLData1.left =  new FormAttachment(0, 1000, 63);
				importCampaignLabelLData1.top =  new FormAttachment(0, 1000, 18);
				importCampaignLabelLData1.width = 169;
				importCampaignLabelLData1.height = 15;
				importCampaignLabel.setLayoutData(importCampaignLabelLData1);
				importCampaignLabel.setText("Import a campaign");
				importCampaignLabel.setBackground(SWTResourceManager.getColor(255,238,219));
			}
			{
				exportCampaignLabel = new Label(dialogShell, SWT.CENTER);
				FormData exportCampaignLabelLData = new FormData();
				exportCampaignLabelLData.width = 362;
				exportCampaignLabelLData.height = 15;
				exportCampaignLabelLData.left =  new FormAttachment(0, 1000, 12);
				exportCampaignLabelLData.top =  new FormAttachment(0, 1000, 70);
				FormData exportCampaignLabelLData1 = new FormData();
				exportCampaignLabelLData1.left =  new FormAttachment(0, 1000, 58);
				exportCampaignLabelLData1.top =  new FormAttachment(0, 1000, 76);
				exportCampaignLabelLData1.width = 176;
				exportCampaignLabelLData1.height = 15;
				exportCampaignLabel.setLayoutData(exportCampaignLabelLData1);
				exportCampaignLabel.setText("Export a campaign");
				exportCampaignLabel.setBackground(SWTResourceManager.getColor(255,238,219));
			}
			dialogShell.layout();
			dialogShell.pack();			
			dialogShell.setSize(286, 328);
			dialogShell.setLocation(getParent().toDisplay(100, 0));
			dialogShell.open();
			Display display = dialogShell.getDisplay();
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
				display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
