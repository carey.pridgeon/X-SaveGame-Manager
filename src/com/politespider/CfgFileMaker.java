package com.politespider;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CfgFileMaker {
	public static void newCfgFile (String fname) {
		BufferedWriter xmlOut = null;
		File fName = new File(fname);
		try {
			xmlOut = new BufferedWriter(new FileWriter(fName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			xmlOut.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			xmlOut.write("<root xsi:noNamespaceSchemaLocation = \"X3SGMConfig.xsd\" xmlns:xsi = \"http://www.w3.org/2001/XMLSchema-instance\">\n");
			xmlOut.write("    <saveFolderPath><![CDATA[standard]]></saveFolderPath>\n");
			xmlOut.write("</root>");
			xmlOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
