package com.politespider;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.swt.widgets.MessageBox;

public class Globals {
	
	//#########
	// To build the X3: Reunion version of the savegame manager, uncomment this block
	//#########
	//public static final String saveFolderNotFound = "X3:R savegame folder not found.\n";
	//public static final String mainWIndowTitle = "X3:Reunion Savegame Manager";
	//public static final String aboutVersionInfo = "The X3:Reunion Savegame Manager\nVersion 1.h\n";
	//public static final String gameSaveFolderLocation = "\\Egosoft\\X3\\save\\";

	//#########
	// To build the X3: Terran Conflict version of the savegame manager, uncomment this block
	//#########
	//public static final String saveFolderNotFound = "X3:TC savegame folder not found.\n";
	//public static final String mainWIndowTitle = "X3:Terran Conflict Savegame Manager";
	//public static final String aboutVersionInfo = "The X3:Terran Conflict Savegame Manager\nVersion 1.h\n";
	//public static final String gameSaveFolderLocation = "\\Egosoft\\X3TC\\save\\";	
	
	//!!!!!!!!!!!!!!!
	// make sure you only have one of the above uncommented at a time. 
	//!!!!!!!!!!!!!!!

	public static final String saveFolderNotFound = "X3:TC savegame folder not found.\n";
	public static final String mainWIndowTitle = "X3:Terran Conflict Savegame Manager";
	public static final String aboutVersionInfo = "The X3:Terran Conflict Savegame Manager\nVersion 1.i\n";
	public static final String gameSaveFolderLocation = "\\Egosoft\\X3TC\\save\\";	
	

	public static double campaignFileversion = 1.1;
	public static double vaultFileversion = 1.0;
	public static final String defaultLogEntry = "\n--------------------\nLog entry: <lognum>\nDay: \n\n\n\n";
	public static final String lognum = "<lognum>";
	public static final String datetime = "<datetime>";
	public static final String pilotsLogFileName ="Pilots log file.txt";
	public static final String exportText = "Select the campaign to export";
	public static final String selectCampaignText = "Select the campaign to activate";
	public static final String archiveCampaignText = "Select the campaign to store";
	public static final String campaignFileName = "campaign file.xml";
	public static final String vaultFileName = "vault file.xml";

	public static ArrayList<File> saveGameSet;
	public static ArrayList<File> pickedSaveGameSet;
	public static ArrayList<CampaignInfo> allCampaigns;
	public static ArrayList<CampaignInfo> vaultContents;
	public static CampaignInfo activeCampaign;
	public static CampaignInfo newCampaign;
	public static CampaignInfo forkedCampaign;
	public static CampaignInfo renameCampaign;
	public static CampaignInfo newVaultEntry;

	public static CampaignInfo selectedCampaign;
	public static CampaignInfo importedCampaign;
	public static boolean dataEntered;
	public static boolean logTemplateLoaded;
	public static int currentCampaignIndex;
	public static int pickedCampaignIndex;
	public static int selectedCampaignIndex;
	public static int vaultSelectedIndex;
	public static int deleteSelectedIndex;
	public static int startingLogNumber = 1;

	public static String[] campaignFileExtensions= {"sav","xml"};
	public static String[] saveFileExtension= {"sav"};
	public static String[] infoFileExtension= {"xml"};
	public static String userHome;	
	public static String specifiedSaveFolder;
	public static String standard = "standard";
	public static String  appHome;	
	public static String campaignSelectBoxText;
	public static String campaignfolderNAME;
	public static String vaultfolderNAME;
	public static String egosoftFolder;
	public static String tempFolder;
	public static String saveFolder;
	public static String gameLocation;
	public static String campaignNameStorage;
	public static String commentStorage;
	public static String newCampaignName;
	
	public static MessageBox mBox;
}
