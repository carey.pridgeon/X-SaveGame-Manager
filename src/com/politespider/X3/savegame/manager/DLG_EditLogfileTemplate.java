package com.politespider.X3.savegame.manager;
import com.cloudgarden.resource.SWTResourceManager;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;




public class DLG_EditLogfileTemplate extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private StyledText templateEditText;
	private Button defaultReset;
	private Label label1;
	private Button cancelButton;
	private Button saveChangesButton;
	private StyledText instructionsText;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_EditLogfileTemplate inst = new DLG_EditLogfileTemplate(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_EditLogfileTemplate(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			dialogShell.setLayout(new FormLayout());
			{
				cancelButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData cancelButtonLData = new FormData();
				cancelButtonLData.width = 123;
				cancelButtonLData.height = 25;
				cancelButtonLData.left =  new FormAttachment(0, 1000, 215);
				cancelButtonLData.top =  new FormAttachment(0, 1000, 279);
				cancelButton.setLayoutData(cancelButtonLData);
				cancelButton.setText("Close without saving");
				cancelButton.setToolTipText("Abandon your changes and return to the log entry dialog");
				cancelButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						dialogShell.dispose();
					}
				});
			}
			{
				FormData label1LData = new FormData();
				label1LData.width = 67;
				label1LData.height = 15;
				label1LData.left =  new FormAttachment(0, 1000, 454);
				label1LData.top =  new FormAttachment(0, 1000, 264);
				label1 = new Label(dialogShell, SWT.NONE);
				label1.setLayoutData(label1LData);
				label1.setBackground(SWTResourceManager.getColor(255,238,219));
				label1.setEnabled(false);
			}
			{
				saveChangesButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData saveChangesButtonLData = new FormData();
				saveChangesButtonLData.width = 85;
				saveChangesButtonLData.height = 25;
				saveChangesButtonLData.left =  new FormAttachment(0, 1000, 118);
				saveChangesButtonLData.top =  new FormAttachment(0, 1000, 279);
				saveChangesButton.setLayoutData(saveChangesButtonLData);
				saveChangesButton.setText("Save Changes");
				saveChangesButton.setToolTipText("Save the revised log entry template and return to the log entry dialog");
				saveChangesButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						Globals.activeCampaign.setLogEntryTemplate(templateEditText.getText());
						CampaignFileHandling.newCampaignFile(Globals.saveFolder+Globals.campaignFileName,Globals.activeCampaign.getCampaignName(), Globals.activeCampaign.getLogFile(),Globals.activeCampaign.getLogEntryNumber(),Globals.activeCampaign.getLogEntryTemplate());
						dialogShell.dispose();
					}
				});
			}
			{
				FormData instructionsTextLData = new FormData();
				instructionsTextLData.width = 231;
				instructionsTextLData.height = 273;
				instructionsTextLData.left =  new FormAttachment(0, 1000, 0);
				instructionsTextLData.top =  new FormAttachment(0, 1000, 0);
				instructionsText = new StyledText(dialogShell, SWT.READ_ONLY | SWT.WRAP);
				instructionsText.setLayoutData(instructionsTextLData);
				instructionsText.setText("The text box on the right contains the current logfile entry template.\n\nThere are some predefined tokens you can use to add additional information to your template:\n\n<lognum>  \nAdds the current log entry number to your template\n<datetime>\nAdds the current date and time in short format.\n\nTo revert to the default logfile entry, press the ''Reset to default' button. To save your changes, click 'Save Changes'.");
				instructionsText.setBackground(SWTResourceManager.getColor(255,238,219));
				instructionsText.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				templateEditText = new StyledText(dialogShell, SWT.NONE);
				FormData templateEditTextLData = new FormData();
				templateEditTextLData.width = 226;
				templateEditTextLData.height = 235;
				templateEditTextLData.left =  new FormAttachment(0, 1000, 238);
				templateEditTextLData.top =  new FormAttachment(0, 1000, 6);
				templateEditText.setLayoutData(templateEditTextLData);
				templateEditText.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				defaultReset = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				defaultReset.setText("Reset to Default");
				FormData defaultResetLData = new FormData();
				defaultResetLData.width = 100;
				defaultResetLData.height = 25;
				defaultResetLData.left =  new FormAttachment(0, 1000, 6);
				defaultResetLData.top =  new FormAttachment(0, 1000, 279);
				defaultReset.setLayoutData(defaultResetLData);
				defaultReset.setToolTipText("Go back to using the default template");
				defaultReset.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						Globals.activeCampaign.setLogEntryTemplate(Globals.defaultLogEntry);
						templateEditText.setText(Globals.defaultLogEntry);
						CampaignFileHandling.newCampaignFile(Globals.saveFolder+Globals.campaignFileName,Globals.activeCampaign.getCampaignName(), Globals.activeCampaign.getLogFile(),Globals.activeCampaign.getLogEntryNumber(),Globals.activeCampaign.getLogEntryTemplate());
					}
				});
			}
			dialogShell.layout();
			dialogShell.pack();			
			dialogShell.setSize(489, 347);
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));
			dialogShell.setLocation(getParent().toDisplay(100,0));
			dialogShell.open();
			Display display = dialogShell.getDisplay();
			templateEditText.setText(Globals.activeCampaign.getLogEntryTemplate());
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
				display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
