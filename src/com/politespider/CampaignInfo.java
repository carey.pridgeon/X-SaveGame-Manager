package com.politespider;

public class CampaignInfo {
	private double X3SGMversion;
	private String campaignName;
	private String logFile;
	private String logEntryTemplate;
	private int logEntryNumber;
	
	public String getLogEntryTemplate() {
		return logEntryTemplate;
	}
	
	public void setLogEntryTemplate(String logEntryTemplate) {
		this.logEntryTemplate = logEntryTemplate;
	}
	
	public int getLogEntryNumber() {
		return logEntryNumber;
	}
	
	public void setLogEntryNumber(int logEntryNumber) {
		this.logEntryNumber = logEntryNumber;
	}
	
	public double getX3SGMversion() {
		return X3SGMversion;
	}
	
	public void setX3SGMversion(double mversion) {
		X3SGMversion = mversion;
	}
	
	public String getCampaignName() {
		return campaignName;
	}
	
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	
	public String getLogFile() {
		return logFile;
	}
	
	public void setLogFile(String comment) {
		this.logFile = comment;
	}
}
