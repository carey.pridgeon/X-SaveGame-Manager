package com.politespider;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.cloudgarden.resource.SWTResourceManager;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;



public class DLG_PilotsLogEditor extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private StyledText logDisplay;
	private Button exportLogButton;
	private Label label2;
	private Text LogCountValueChange;
	private Button ResetLogCountButton;
	private Button modifyTemplateButton;
	private Button cancelButton;
	private Button doneButton;
	private Button saveLogEntryButton;
	private Button addLogEntryButton;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_PilotsLogEditor inst = new DLG_PilotsLogEditor(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_PilotsLogEditor(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			
			dialogShell.setLayout(new FormLayout());
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));
			{
				exportLogButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData exportLogButtonLData = new FormData();
				exportLogButtonLData.width = 134;
				exportLogButtonLData.height = 25;
				exportLogButtonLData.left =  new FormAttachment(0, 1000, 274);
				exportLogButtonLData.top =  new FormAttachment(0, 1000, 216);
				exportLogButton.setLayoutData(exportLogButtonLData);
				exportLogButton.setText("Export Pilots Log");
				exportLogButton.setToolTipText("Save the Pilots log to a text file");
				exportLogButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent evt) {
						FileDialog dlg = new FileDialog(dialogShell.getShell(), SWT.SAVE);
						dlg.setFileName(Globals.pilotsLogFileName);
						String fileName = dlg.open();
						if (fileName != null) {

							BufferedWriter logFileOut = null;
							File fName = new File(fileName);
							try {
								logFileOut = new BufferedWriter(new FileWriter(fName));
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							try {
								logFileOut.write(FileHandling.convertToCrossPlatformNewline(Globals.activeCampaign.getLogFile()));
								logFileOut.close();
							} catch (IOException e) {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION|SWT.OK);
								Globals.mBox.setMessage("Unable to write the pilots log file to the selected destination.");
								Globals.mBox.open();
							}
						}
					}
				});
			}
			{
				LogCountValueChange = new Text(dialogShell, SWT.BORDER);
				FormData LogCountValueChangeLData = new FormData();
				LogCountValueChangeLData.width = 19;
				LogCountValueChangeLData.height = 12;
				LogCountValueChangeLData.left =  new FormAttachment(0, 1000, 292);
				LogCountValueChangeLData.top =  new FormAttachment(0, 1000, 170);
				LogCountValueChange.setLayoutData(LogCountValueChangeLData);
				LogCountValueChange.setText("0");
				
			}
			{
				ResetLogCountButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData ResetLogCountButtonLData = new FormData();
				ResetLogCountButtonLData.width = 52;
				ResetLogCountButtonLData.height = 22;
				ResetLogCountButtonLData.left =  new FormAttachment(0, 1000, 329);
				ResetLogCountButtonLData.top =  new FormAttachment(0, 1000, 170);
				ResetLogCountButton.setLayoutData(ResetLogCountButtonLData);
				ResetLogCountButton.setText("Apply");
				ResetLogCountButton.setToolTipText("Updates the campaign file of the current campaign with this new log entry number");
				ResetLogCountButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						// check to see if the typed value is a valid number
						if (!LogCountValueChange.getText().isEmpty()) {
							try {
								int test =Integer.decode(LogCountValueChange.getText());
								//Looks like it passes, so it can be used to change the log count value
								Globals.activeCampaign.setLogEntryNumber(test);
								CampaignFileHandling.newCampaignFile(Globals.saveFolder+Globals.campaignFileName,Globals.activeCampaign.getCampaignName(), Globals.activeCampaign.getLogFile(),Globals.activeCampaign.getLogEntryNumber(),Globals.activeCampaign.getLogEntryTemplate());
							} catch (NumberFormatException e1) {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
								Globals.mBox.setMessage("Apparently that wasn't a genuine integer, so it can't be used.");
								Globals.mBox.open();
							}
						}
					}
				});
			}
			{
				modifyTemplateButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData modifyTemplateButtonLData = new FormData();
				modifyTemplateButtonLData.width = 118;
				modifyTemplateButtonLData.height = 25;
				modifyTemplateButtonLData.left =  new FormAttachment(0, 1000, 272);
				modifyTemplateButtonLData.top =  new FormAttachment(0, 1000, 107);
				modifyTemplateButton.setLayoutData(modifyTemplateButtonLData);
				modifyTemplateButton.setText("edit log template");
				modifyTemplateButton.setToolTipText("Alter the current log entry template");
				modifyTemplateButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						DLG_EditLogfileTemplate editT = new DLG_EditLogfileTemplate(dialogShell.getShell(),SWT.APPLICATION_MODAL);
						editT.open();
					}
				});
				
				
			}
			{
				cancelButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData cancelButtonLData = new FormData();
				cancelButtonLData.width = 57;
				cancelButtonLData.height = 25;
				cancelButtonLData.left =  new FormAttachment(0, 1000, 272);
				cancelButtonLData.top =  new FormAttachment(0, 1000, 70);
				cancelButton.setLayoutData(cancelButtonLData);
				cancelButton.setText("cancel");
				cancelButton.setEnabled(false);
				cancelButton.setToolTipText("Abandon the current log file editing session");
				cancelButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						saveLogEntryButton.setEnabled(false);
						cancelButton.setEnabled(false);
						addLogEntryButton.setEnabled(true);
						logDisplay.setText(Globals.activeCampaign.getLogFile());
						logDisplay.setEditable(false);
					}
				});
			}
			{
				doneButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData doneButtonLData = new FormData();
				doneButtonLData.width = 54;
				doneButtonLData.height = 25;
				doneButtonLData.left =  new FormAttachment(0, 1000, 354);
				doneButtonLData.top =  new FormAttachment(0, 1000, 253);
				doneButton.setLayoutData(doneButtonLData);
				doneButton.setText("Done");
				doneButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						saveLogEntryButton.setEnabled(false);
						cancelButton.setEnabled(false);
						addLogEntryButton.setEnabled(true);
						logDisplay.setEditable(false);
						dialogShell.dispose();
					}
				});
			}
			{
				saveLogEntryButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData saveLogEntryButtonLData = new FormData();
				saveLogEntryButtonLData.width = 58;
				saveLogEntryButtonLData.height = 25;
				saveLogEntryButtonLData.left =  new FormAttachment(0, 1000, 272);
				saveLogEntryButtonLData.top =  new FormAttachment(0, 1000, 39);
				saveLogEntryButton.setLayoutData(saveLogEntryButtonLData);
				saveLogEntryButton.setText("Save");
				saveLogEntryButton.setEnabled(false);
				saveLogEntryButton.setToolTipText("Save the current entry to the pilots log file");
				saveLogEntryButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						saveLogEntryButton.setEnabled(false);
						logDisplay.setEditable(false);
						addLogEntryButton.setEnabled(true);
						//update the campaign info file and the copy in memory as well.
						Globals.activeCampaign.setLogEntryNumber(Globals.activeCampaign.getLogEntryNumber()+1);
						Globals.activeCampaign.setLogFile(logDisplay.getText());
						CampaignFileHandling.newCampaignFile(Globals.saveFolder+Globals.campaignFileName,Globals.activeCampaign.getCampaignName(), Globals.activeCampaign.getLogFile(),Globals.activeCampaign.getLogEntryNumber(),Globals.activeCampaign.getLogEntryTemplate());
					}
				});
			}
			{
				addLogEntryButton = new Button(dialogShell, SWT.PUSH | SWT.FLAT | SWT.CENTER);
				FormData addLogEntryButtonLData = new FormData();
				addLogEntryButtonLData.width = 121;
				addLogEntryButtonLData.height = 25;
				addLogEntryButtonLData.left =  new FormAttachment(0, 1000, 272);
				addLogEntryButtonLData.top =  new FormAttachment(0, 1000, 8);
				addLogEntryButton.setLayoutData(addLogEntryButtonLData);
				addLogEntryButton.setText("New Log Entry");
				addLogEntryButton.setBackground(SWTResourceManager.getColor(255,238,219));
				addLogEntryButton.setToolTipText("Make a new entry into this campaigns log");
				addLogEntryButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						addLogEntryButton.setEnabled(false);
						logDisplay.setEditable(true);
						logDisplay.setText(CampaignUtils.processLogFileTemplate(Globals.activeCampaign.getLogEntryTemplate()+Globals.activeCampaign.getLogFile()));
						saveLogEntryButton.setEnabled(true);
						cancelButton.setEnabled(true);
					}
					
				});
			}
			{
				logDisplay = new StyledText(dialogShell, SWT.READ_ONLY | SWT.WRAP | SWT.V_SCROLL | SWT.BORDER);
				FormData logDisplayLData = new FormData();
				logDisplayLData.width = 236;
				logDisplayLData.height = 256;
				logDisplayLData.left =  new FormAttachment(0, 1000, 10);
				logDisplayLData.top =  new FormAttachment(0, 1000, 12);
				logDisplay.setLayoutData(logDisplayLData);
				logDisplay.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				label2 = new Label(dialogShell, SWT.NONE);
				FormData label2LData = new FormData();
				label2LData.left =  new FormAttachment(0, 1000, 274);
				label2LData.top =  new FormAttachment(0, 1000, 149);
				label2LData.width = 113;
				label2LData.height = 15;
				label2.setLayoutData(label2LData);
				label2.setText("Set Log Number to:");
				label2.setBackground(SWTResourceManager.getColor(255,238,219));
				label2.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			dialogShell.layout();
			dialogShell.pack();			
			dialogShell.setSize(424, 316);
			dialogShell.setLocation(getParent().toDisplay(100, 0));
			dialogShell.open();
			Display display = dialogShell.getDisplay();
			logDisplay.setText(Globals.activeCampaign.getLogFile());
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
				display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
