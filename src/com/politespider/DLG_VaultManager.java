package com.politespider;
import java.io.File;
import java.util.ArrayList;
import com.cloudgarden.resource.SWTResourceManager;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;




public class DLG_VaultManager extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private Button sendToVaultButton;
	private Button vaultViewButton;
	private Button deleteFromVaultButton;
	private Button doneButton;
	private Label progressLabel;
	private ProgressBar progressBar;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_VaultManager inst = new DLG_VaultManager(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_VaultManager(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			
			dialogShell.setLayout(new FormLayout());
			dialogShell.setText("Vault Manager");
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));

			dialogShell.layout();
			dialogShell.pack();	
			dialogShell.setSize(205, 245);

			{
				doneButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData doneButtonLData = new FormData();
				doneButtonLData.width = 40;
				doneButtonLData.height = 25;
				doneButtonLData.left =  new FormAttachment(0, 1000, 76);
				doneButtonLData.top =  new FormAttachment(0, 1000, 144);
				doneButton.setLayoutData(doneButtonLData);
				doneButton.setText("Done");
				doneButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						dialogShell.dispose();
					}
				});
			}
			{
				FormData progressLabelLData = new FormData();
				progressLabelLData.width = 192;
				progressLabelLData.height = 15;
				progressLabelLData.left =  new FormAttachment(0, 1000, 0);
				progressLabelLData.top =  new FormAttachment(0, 1000, 175);
				progressLabel = new Label(dialogShell, SWT.NONE);
				progressLabel.setLayoutData(progressLabelLData);
				progressLabel.setBackground(SWTResourceManager.getColor(255,238,219));
				progressLabel.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				FormData progressBarLData = new FormData();
				progressBarLData.width = 197;
				progressBarLData.height = 17;
				progressBarLData.left =  new FormAttachment(0, 1000, 0);
				progressBarLData.top =  new FormAttachment(0, 1000, 190);
				progressBar = new ProgressBar(dialogShell, SWT.NONE);
				progressBar.setLayoutData(progressBarLData);
			}
			{
				deleteFromVaultButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData deleteFromVaultButtonLData = new FormData();
				deleteFromVaultButtonLData.width = 173;
				deleteFromVaultButtonLData.height = 25;
				deleteFromVaultButtonLData.left =  new FormAttachment(0, 1000, 12);
				deleteFromVaultButtonLData.top =  new FormAttachment(0, 1000, 97);
				deleteFromVaultButton.setLayoutData(deleteFromVaultButtonLData);
				deleteFromVaultButton.setText("Delete a Vault Entry");
				deleteFromVaultButton.setToolTipText("This allows you to delete campaigns stored in the vault. Deletions cannot be undone!");
				deleteFromVaultButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						Globals.vaultSelectedIndex = -1;
						Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_WARNING
						| SWT.YES | SWT.NO);
						Globals.mBox.setMessage("Are you absolutelly sure you want to delete a campaign from the Vault? Deletions are permenant!\n\n(You will be asked once again before final deletion)");
						int rx = Globals.mBox.open(); 
						if (rx ==  SWT.NO) {
							dialogShell.dispose();
						} else {
							DLG_VaultEntrySelect vsBox = new DLG_VaultEntrySelect(dialogShell,SWT.APPLICATION_MODAL);
							vsBox.open();
							if (Globals.vaultSelectedIndex==-1) {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION |SWT.OK);
								Globals.mBox.setMessage("No archived campaign was selected, aborting this operation");
								Globals.mBox.open();
							} else {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_WARNING |SWT.YES|SWT.NO);
								Globals.mBox.setMessage("You have chosen to delete the following archived campaign\n from the vault:\n\n'"+Globals.vaultContents.get(Globals.vaultSelectedIndex).getCampaignName()+"'\n\nAre you absolutelly sure you want to do this?\n");
								int reply = Globals.mBox.open();
								if (reply==SWT.YES) {
									boolean result = FileHandling.deleteCampaignFolder(Globals.vaultfolderNAME+Globals.vaultContents.get(Globals.vaultSelectedIndex).getCampaignName());
									if (result==true) {
										Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION |SWT.OK);
										Globals.mBox.setMessage("Deletion succesful\n");
										Globals.mBox.open();
										progressLabel.setText("Deletion succesfull.");
										// revise the list of the archived campaigns 
										Globals.vaultContents.clear();
										String[] vaultContent = FileHandling.listDir(Globals.vaultfolderNAME);
										for (int t=0;t<vaultContent.length;t++) {
											try {
												Globals.vaultContents.add(VaultFileHandling.loadVaultFile(Globals.vaultfolderNAME+vaultContent[t]+"/"+Globals.vaultFileName));
											} catch (Exception e1) {
												Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
												Globals.mBox.setMessage("Fatal Error: The vault data file 'vault file.xml' is missing for the following archived Campaign: "+Globals.vaultfolderNAME+vaultContent[t]);
												Globals.mBox.open();
												System.exit(0);
											}
										}
									} else {
										Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
										Globals.mBox.setMessage("Unable to delete specified vault entry. Please open the vault and delete it manually.");
										Globals.mBox.open();
										System.exit(0);
									}
								}
								if (reply==SWT.NO) {
									Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION |SWT.OK);
									Globals.mBox.setMessage("Deletion cancelled.");
									Globals.mBox.open();
									progressLabel.setText("Deletion cancelled");
								}
							}
						}
					}
				});
			}
			{
				vaultViewButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData vaultViewButtonLData = new FormData();
				vaultViewButtonLData.width = 173;
				vaultViewButtonLData.height = 25;
				vaultViewButtonLData.left =  new FormAttachment(0, 1000, 12);
				vaultViewButtonLData.top =  new FormAttachment(0, 1000, 12);
				vaultViewButton.setLayoutData(vaultViewButtonLData);
				vaultViewButton.setText("Open The Vault");
				vaultViewButton.setToolTipText("View the campaigns available in the vault, and create new campaigns using them as a base");
				vaultViewButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						DLG_VaultViewer vVBox = new DLG_VaultViewer(dialogShell.getShell(),SWT.APPLICATION_MODAL);
						vVBox.open();
					}
				});
			}
			{
				sendToVaultButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData sendToVaultButtonLData = new FormData();
				sendToVaultButtonLData.width = 173;
				sendToVaultButtonLData.height = 25;
				sendToVaultButtonLData.left =  new FormAttachment(0, 1000, 12);
				sendToVaultButtonLData.top =  new FormAttachment(0, 1000, 55);
				sendToVaultButton.setLayoutData(sendToVaultButtonLData);
				sendToVaultButton.setText("Add a Campaign to the Vault");
				sendToVaultButton.setToolTipText("Begins the process of adding a copy of one of your existing campaigns to the vault");
				sendToVaultButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						Globals.pickedCampaignIndex=-1;
						Globals.campaignSelectBoxText = Globals.archiveCampaignText;
						DLG_CampaignSelectBox cSBox = new DLG_CampaignSelectBox(dialogShell.getShell(),SWT.APPLICATION_MODAL);
						cSBox.open();
						if (Globals.pickedCampaignIndex==-1) {
							Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR
							| SWT.OK);
							Globals.mBox.setMessage("No Campaign selected");
							Globals.mBox.open(); 
						} else { //a campaign was picked, move to individual savegame selection
							Globals.saveGameSet = new ArrayList<File>();
							DLG_SaveSelect sSBox = new DLG_SaveSelect(dialogShell.getShell(),SWT.APPLICATION_MODAL);
							sSBox.open();
							if (Globals.saveGameSet.size()==0) {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR
								| SWT.OK);
								Globals.mBox.setMessage("No Savegames selected, aborting operation");
								Globals.mBox.open(); 
							} else {
								// fire up the campaign namer
								DLG_VaultEntrySetup vSBox = new DLG_VaultEntrySetup(dialogShell.getShell(),SWT.APPLICATION_MODAL);
								vSBox.open();
								// we *should* have a set of savegames, a campaign name, and optionally
								// some descriptive text.
								// check the campaign name survived invalid character removal
								if ((Globals.campaignNameStorage ==null)||(Globals.campaignNameStorage.length()==0)) {
									Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("Campaign name not properly set - aborting operation");
									Globals.mBox.open();
								} else {
									Globals.newVaultEntry = new CampaignInfo();
									Globals.newVaultEntry.setCampaignName(Globals.campaignNameStorage);
									Globals.newVaultEntry.setLogFile(Globals.commentStorage);
									FileHandling.createFolder(Globals.vaultfolderNAME+Globals.newVaultEntry.getCampaignName());
									VaultFileHandling.newVaultFile(Globals.vaultfolderNAME+Globals.newVaultEntry.getCampaignName()+"/"+Globals.vaultFileName,Globals.newVaultEntry.getCampaignName(), Globals.newVaultEntry.getLogFile());
									// and copy over the selected savegames
									
									progressBar.setMinimum(0);
									progressBar.setMaximum(Globals.saveGameSet.size());
									for (int i=0;i<Globals.saveGameSet.size();i++) {
										progressLabel.setText("Copying "+Globals.saveGameSet.get(i).toString());
										try {
											FileHandling.copySingleFile(Globals.saveGameSet.get(i).toString(),Globals.vaultfolderNAME+Globals.newVaultEntry.getCampaignName()+"/"+Globals.saveGameSet.get(i).getName().toString());
										} catch (Exception e1) {
											Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
											Globals.mBox.setMessage("Savegame copy operation failed while copying: "+Globals.saveGameSet.get(i).toString()+ ". Copy operation aborted. ");
											Globals.mBox.open();
											System.exit(0);
										}
										progressBar.setSelection(i);
										progressLabel.setText("Vault entry created succesfully.");
										progressBar.setSelection(0);
									}
								}
							}
							// revise the list of the archived campaigns 
							Globals.vaultContents.clear();
							String[] vaultContent = FileHandling.listDir(Globals.vaultfolderNAME);
							for (int t=0;t<vaultContent.length;t++) {
								try {
									Globals.vaultContents.add(VaultFileHandling.loadVaultFile(Globals.vaultfolderNAME+vaultContent[t]+"/"+Globals.vaultFileName));
								} catch (Exception e1) {
									Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("Fatal Error: The vault data file 'vault file.xml' is missing for the following archived Campaign: "+Globals.vaultfolderNAME+vaultContent[t]);
									Globals.mBox.open();
									System.exit(0);
								}
							}
						}
					}
				});
			}
			dialogShell.setLocation(getParent().toDisplay(150, 0));
			dialogShell.open();
			//silently create the Vault folder if it doesn't already exist
			if(!FileHandling.doesFolderExist(Globals.vaultfolderNAME)) {
				FileHandling.createFolder(Globals.vaultfolderNAME);
			}
			
			// Collect up a list of the archived campaigns
			Globals.vaultContents = new ArrayList<CampaignInfo>();
			String[] vaultContent = FileHandling.listDir(Globals.vaultfolderNAME);
			for (int t=0;t<vaultContent.length;t++) {
				try {
					Globals.vaultContents.add(VaultFileHandling.loadVaultFile(Globals.vaultfolderNAME+vaultContent[t]+"/"+Globals.vaultFileName));
				} catch (Exception e) {
					Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
					Globals.mBox.setMessage("Fatal Error: The achived campaign data file 'vault file.xml' is missing for the following archived Campaign: "+Globals.vaultfolderNAME+vaultContent[t]);
					Globals.mBox.open();
					System.exit(0);
				}
			}
			
			Display display = dialogShell.getDisplay();
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
				display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
