package com.politespider.X3.savegame.manager;
import java.io.File;

import com.cloudgarden.resource.SWTResourceManager;

import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;


public class DLG_Delete extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private List deleteSelectList;
	private Button DeleteButton;
	private Button doneButton;
	private Label DeleteLabel;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_Delete inst = new DLG_Delete(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_Delete(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			
			
			dialogShell.setLayout(new FormLayout());
			{
				doneButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData DoneButtonLData = new FormData();
				DoneButtonLData.left =  new FormAttachment(0, 1000, 203);
				DoneButtonLData.top =  new FormAttachment(0, 1000, 244);
				DoneButtonLData.width = 40;
				DoneButtonLData.height = 25;
				doneButton.setLayoutData(DoneButtonLData);
				doneButton.setText("Done");
				doneButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						dialogShell.dispose();
					}
				});
			}
			{
				DeleteButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData DeleteButtonLData = new FormData();
				DeleteButtonLData.left =  new FormAttachment(0, 1000, 91);
				DeleteButtonLData.top =  new FormAttachment(0, 1000, 225);
				DeleteButtonLData.width = 45;
				DeleteButtonLData.height = 25;
				DeleteButton.setLayoutData(DeleteButtonLData);
				DeleteButton.setText("Delete");
				DeleteButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent evt) {
						System.out.println("DeleteButton.widgetSelected, event="+evt);
						if (deleteSelectList.getSelectionIndex() !=-1) {
							Globals.deleteSelectedIndex = deleteSelectList.getSelectionIndex();
							if (Globals.deleteSelectedIndex  == Globals.currentCampaignIndex) {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
								Globals.mBox.setMessage("You Cannot Delete The Active Campaign.");
								Globals.mBox.open();
								
							} else {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK|SWT.CANCEL);
								Globals.mBox.setMessage("You Have Selected '"+Globals.allCampaigns.get(Globals.deleteSelectedIndex).getCampaignName()+"'. To delete this Campaign, click 'OK'.\n To Abort, click CANCEL'.\n WARNING! Deletion cannot be undone, I mean it....");
								int response = Globals.mBox.open();
								File[] dList = null;
								if (response == SWT.OK) {
									dList = FileHandling.listDirectoryContent(Globals.campaignfolderNAME+Globals.allCampaigns.get(Globals.deleteSelectedIndex).getCampaignName(),Globals.campaignFileExtensions,false);
									boolean output = true;
									boolean reply = false;
									for (int r = 0; r<dList.length;r++) {
										reply = FileHandling.fileDelete(dList[r].toString());
										if (!reply) {
											output = false;
										}
									}
									reply = FileHandling.fileDelete(Globals.campaignfolderNAME+Globals.allCampaigns.get(Globals.deleteSelectedIndex).getCampaignName());
									if (!reply) {
										output = false;
									}
									if (output) {
										Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK|SWT.CANCEL);
										Globals.mBox.setMessage("Campaign '"+Globals.allCampaigns.get(Globals.deleteSelectedIndex).getCampaignName()+"' deleted.");
										Globals.mBox.open();
									} else {
										Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK|SWT.CANCEL);
										Globals.mBox.setMessage("Campaign '"+Globals.allCampaigns.get(Globals.deleteSelectedIndex).getCampaignName()+"' deleted with errors, some files may remain.");
										Globals.mBox.open();								}
									}
								// repopulate the campaigns list
								Globals.allCampaigns.clear();
								String[] rList = FileHandling.listDir(Globals.campaignfolderNAME);
								for (int q=0;q<rList.length;q++) {
									Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(Globals.campaignfolderNAME+rList[q]+"/"+Globals.campaignFileName));
								}
								deleteSelectList.removeAll();
								for (int r=0;r<rList.length;r++) {
									deleteSelectList.add(Globals.allCampaigns.get(r).getCampaignName());
									if(Globals.allCampaigns.get(r).getCampaignName().equals(Globals.activeCampaign.getCampaignName())) {
										Globals.currentCampaignIndex = r;
										Globals.selectedCampaignIndex = r;
									}
								}
								}

						}
					}
				});
			}
			{
				DeleteLabel = new Label(dialogShell, SWT.NONE);
				FormData DeleteLabelLData = new FormData();
				DeleteLabelLData.left =  new FormAttachment(0, 1000, 6);
				DeleteLabelLData.top =  new FormAttachment(0, 1000, 12);
				DeleteLabelLData.width = 231;
				DeleteLabelLData.height = 15;
				DeleteLabel.setLayoutData(DeleteLabelLData);
				DeleteLabel.setText("Choose the Campaign You want to Delete");
				DeleteLabel.setBackground(SWTResourceManager.getColor(255, 238, 219));
				DeleteLabel.setFont(SWTResourceManager.getFont("Segoe UI", 9, 1, false, false));
			}
			{
				deleteSelectList = new List(dialogShell, SWT.V_SCROLL | SWT.BORDER);
				deleteSelectList.setFont(SWTResourceManager.getFont("Segoe UI",9,3,false,false));
				FormData campaignsListLData = new FormData();
				campaignsListLData.left =  new FormAttachment(0, 1000, 4);
				campaignsListLData.top =  new FormAttachment(0, 1000, 39);
				campaignsListLData.width = 208;
				campaignsListLData.height = 174;
				deleteSelectList.setLayoutData(campaignsListLData);
				deleteSelectList.setToolTipText("All the campaigns you currently have are listed here");
				deleteSelectList.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent arg0) {
							if (deleteSelectList.getSelectionIndex() !=-1) {
								Globals.deleteSelectedIndex = deleteSelectList.getSelectionIndex();
							}
					}
				});
			}
			dialogShell.layout();
			dialogShell.pack();			
			dialogShell.setLocation(getParent().toDisplay(100, 0));
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));
			dialogShell.open();
			//populate the list
			//populate the list
			for (int i=0;i<Globals.allCampaigns.size();i++) {
				deleteSelectList.add(Globals.allCampaigns.get(i).getCampaignName());
			}
			Display display = dialogShell.getDisplay();
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
					display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
