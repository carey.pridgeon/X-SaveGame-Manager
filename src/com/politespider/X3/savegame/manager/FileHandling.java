package com.politespider.X3.savegame.manager;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
public class FileHandling {
	
	public static boolean copySingleFile (String srcFile, String destination) {
		try {
			FileUtils.copyFile(new File(srcFile),new File(destination),true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static boolean doesFolderExist(String foldername) {
		File f=new File(foldername);  
		if(f.isDirectory()) {
			return true;
		}
		return false;
	}
	
	public static boolean doesFileExist(String fname) {
		File f=new File(fname);  
		if(f.exists()) {
			return true;
		}
		return false;
	}
	
	public static boolean createFolder(String folderName) {
		try {
			FileUtils.forceMkdir(new File(folderName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static String[] listDir (String folderName) {
		File dir = new File(folderName);
		return dir.list(DirectoryFileFilter.INSTANCE);
	}

	public  static File[] listDirectoryContent(String dir,String[] extensions, boolean recurseOpt) {
		return FileUtils.convertFileCollectionToFileArray(FileUtils.listFiles(new File(dir),extensions, recurseOpt));
	}
	
	public  static File[] listDirectories(String dir) {
		return FileUtils.convertFileCollectionToFileArray(FileUtils.listFiles(new File(dir),DirectoryFileFilter.INSTANCE,DirectoryFileFilter.INSTANCE));
	}	
	

	public static boolean emptyDirectory (String dir) {
		try {
			File[] fileList = FileHandling.listDirectoryContent(dir,Globals.campaignFileExtensions,false);
			if (fileList.length!=0) {
				for (int i=0;i<fileList.length;i++) {
					FileUtils.deleteQuietly(fileList[i]);
				}
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean deleteCampaignFolder (String dir) {
		try {
			FileUtils.deleteDirectory(new File(dir));
			return true;
		} catch (Exception e) {
			return false;
		}
	}	
	
	public static File[] getFolderContent(String folderPath) {
		File[] contents = null;
		File sDir = new File(folderPath);
		if( sDir.exists() && sDir.isDirectory()){
			contents = sDir.listFiles();
		} else {
			System.exit(0);
		}
		return contents;
	}

	public static String filenameCleaner(String s) {
		StringBuffer sb = new StringBuffer();
		try {
			for (int i = 0; i < s.length(); i++) {
				if ((!(s.charAt(i) == '\\'))&&(!(s.charAt(i) == '/'))) {
					sb = sb.append(s.charAt(i));
				}
			}
		} catch (Exception e) {
		} 
		return sb.toString();
	}
	
	public static boolean isFilenameAlreadyInUse(String filename) {
		for (int x=0;x<Globals.allCampaigns.size();x++) {
			if (Globals.allCampaigns.get(x).getCampaignName().toLowerCase().equals(filename.toLowerCase())) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean copySingleCampaignFolder(String srcfolderName,String destFolderName) {
		try {
			FileUtils.copyDirectory(new File(srcfolderName),new File(destFolderName),true);
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	public static String convertToCrossPlatformNewline(String input) {
		String result = new String();
		for (int i=0;i<input.length();i++) {
			if (input.charAt(i)=='\n'){
				result = result.concat(""+ "\r\n");
			} else {
				result = result.concat(""+ input.charAt(i));
			}
		}
		return result;
	}
	
	public static boolean fileDelete (String fileName) {
		    File f = new File(fileName);
		    boolean success = f.delete();
		    if (!success) {
			      return false;
		    } else {
		    	  return true;
		    }
	}
	

	
}
