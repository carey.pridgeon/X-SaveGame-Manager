package com.politespider;

import java.io.File;
import com.cloudgarden.resource.SWTResourceManager;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class DLG_VaultViewer extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private List vaultList;
	private Label vaultContentLabel;
	private Button createButton;
	private Label createProgresInfo;
	private Label vaultEntryInfoLabel;
	private Button doneButton;
	private StyledText makeNewFromVaultEntry;
	private StyledText vaultEntryCommentView;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_VaultViewer inst = new DLG_VaultViewer(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_VaultViewer(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			
			
			dialogShell.setLayout(new FormLayout());
			{
				createProgresInfo = new Label(dialogShell, SWT.NONE);
				FormData createProgresInfoLData = new FormData();
				createProgresInfoLData.width = 375;
				createProgresInfoLData.height = 15;
				createProgresInfoLData.left =  new FormAttachment(0, 1000, 6);
				createProgresInfoLData.top =  new FormAttachment(0, 1000, 277);
				createProgresInfo.setLayoutData(createProgresInfoLData);
				createProgresInfo.setBackground(SWTResourceManager.getColor(255,238,219));
			}
			{
				vaultEntryInfoLabel = new Label(dialogShell, SWT.NONE);
				FormData vaultEntryInfoLabelLData = new FormData();
				vaultEntryInfoLabelLData.width = 172;
				vaultEntryInfoLabelLData.height = 15;
				vaultEntryInfoLabelLData.left =  new FormAttachment(0, 1000, 209);
				vaultEntryInfoLabelLData.top =  new FormAttachment(0, 1000, 12);
				vaultEntryInfoLabel.setLayoutData(vaultEntryInfoLabelLData);
				vaultEntryInfoLabel.setText("Information");
				vaultEntryInfoLabel.setBackground(SWTResourceManager.getColor(255,238,219));
				vaultEntryInfoLabel.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				doneButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData doneButtonLData = new FormData();
				doneButtonLData.width = 57;
				doneButtonLData.height = 25;
				doneButtonLData.left =  new FormAttachment(0, 1000, 466);
				doneButtonLData.top =  new FormAttachment(0, 1000, 258);
				doneButton.setLayoutData(doneButtonLData);
				doneButton.setText("Done");
				doneButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						dialogShell.dispose();
					}
				});
			}
			{
				createButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData createButtonLData = new FormData();
				createButtonLData.width = 52;
				createButtonLData.height = 25;
				createButtonLData.left =  new FormAttachment(0, 1000, 387);
				createButtonLData.top =  new FormAttachment(0, 1000, 246);
				createButton.setLayoutData(createButtonLData);
				createButton.setText("Create");
				createButton.setToolTipText("Create a new vault entry using all or part of an existing campaign");
				createButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						Globals.vaultSelectedIndex = -1;
						DLG_VaultEntrySelect vsBox = new DLG_VaultEntrySelect(dialogShell,SWT.APPLICATION_MODAL);
						vsBox.open();
						if (Globals.vaultSelectedIndex==-1) {
							Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION |SWT.OK);
							Globals.mBox.setMessage("No archived campaign was selected, aborting this operation");
							Globals.mBox.open();
						} else {
							// scan the selected vault folder for savegame files
							File[] collectUp = FileHandling.listDirectoryContent(Globals.vaultfolderNAME+Globals.vaultContents.get(Globals.vaultSelectedIndex).getCampaignName(),Globals.saveFileExtension,false);
							if (collectUp.length ==0) {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
								Globals.mBox.setMessage("Fatal Error: No X3:TC save files found in specified vault folder:\n"+Globals.vaultfolderNAME+Globals.vaultContents.get(Globals.vaultSelectedIndex).getCampaignName());
								Globals.mBox.open();
								System.exit(0);
							} else {
								// we have our set of savegames, so its time to create a campaign from them. 
								Globals.campaignNameStorage = null;
								Globals.commentStorage = null;
								DLG_CampaignSetup setupBox = new DLG_CampaignSetup(dialogShell.getShell(),1);
								setupBox.open();
								
								//check to see that we have a campaign name specified, if not, exit (null or empty)
								if ((Globals.campaignNameStorage ==null)||(Globals.campaignNameStorage.length()==0)) {
									Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("Campaign name not properly set - aborting operation");
									Globals.mBox.open();
								} else {
									// ok we got this far, the information to create a campaign file exists
									Globals.importedCampaign = new CampaignInfo();
									Globals.importedCampaign.setCampaignName(Globals.campaignNameStorage);
									if (Globals.commentStorage == null) {
										Globals.importedCampaign.setLogFile("");
									} else {
										Globals.importedCampaign.setLogFile(Globals.commentStorage);
									}
									// start off logfile entry numbering
									Globals.importedCampaign.setLogEntryNumber(0);
									createProgresInfo.setText("Please wait, creating campaign...");
									// make the folder we are going to need
									FileHandling.createFolder(Globals.campaignfolderNAME+Globals.importedCampaign.getCampaignName());
									// write the campaign file to it
									CampaignFileHandling.newCampaignFile(Globals.campaignfolderNAME+Globals.importedCampaign.getCampaignName()+"\\"+Globals.campaignFileName, Globals.importedCampaign.getCampaignName(), Globals.importedCampaign.getLogFile(),Globals.importedCampaign.getLogEntryNumber(),Globals.importedCampaign.getLogEntryTemplate());
									//now copy the files over
									for (int i=0;i<collectUp.length;i++) {
										try {
											FileHandling.copySingleFile(collectUp[i].toString(),Globals.campaignfolderNAME+Globals.importedCampaign.getCampaignName()+"/"+collectUp[i].getName().toString());
										} catch (Exception e1) {
											Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR |SWT.OK);
											Globals.mBox.setMessage("Fatal Error Savegame copy operation failed while copying: "+collectUp[i].toString());
											Globals.mBox.open();
											System.exit(0);
										}
									}
									createProgresInfo.setText("Campaign created succesfully");
									
								}
							}
						}
					}
				});
			}
			{
				makeNewFromVaultEntry = new StyledText(dialogShell, SWT.READ_ONLY | SWT.WRAP);
				FormData makeNewFromVaultEntryLData = new FormData();
				makeNewFromVaultEntryLData.width = 124;
				makeNewFromVaultEntryLData.height = 235;
				makeNewFromVaultEntryLData.left =  new FormAttachment(0, 1000, 393);
				makeNewFromVaultEntryLData.top =  new FormAttachment(0, 1000, 5);
				makeNewFromVaultEntry.setLayoutData(makeNewFromVaultEntryLData);
				makeNewFromVaultEntry.setText("To make a new Campaign using one of the archived campaigns stored in the vault as a template, click 'Create'.\n\nThe main campaigns list will refresh once the vault manager is closed.");
				makeNewFromVaultEntry.setBackground(SWTResourceManager.getColor(255,238,219));
				makeNewFromVaultEntry.setEnabled(false);
			}
			{
				FormData vaultEntryComentViewLData = new FormData();
				vaultEntryComentViewLData.width = 171;
				vaultEntryComentViewLData.height = 233;
				vaultEntryComentViewLData.left =  new FormAttachment(0, 1000, 209);
				vaultEntryComentViewLData.top =  new FormAttachment(0, 1000, 38);
				vaultEntryCommentView = new StyledText(dialogShell, SWT.READ_ONLY | SWT.WRAP);
				vaultEntryCommentView.setLayoutData(vaultEntryComentViewLData);
				vaultEntryCommentView.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
				vaultEntryCommentView.setToolTipText("This shows the information for the currently selected vault entry");

			}
			{
				vaultContentLabel = new Label(dialogShell, SWT.NONE);
				FormData vaultContentLabelLData = new FormData();
				vaultContentLabelLData.width = 186;
				vaultContentLabelLData.height = 15;
				vaultContentLabelLData.left =  new FormAttachment(0, 1000, 6);
				vaultContentLabelLData.top =  new FormAttachment(0, 1000, 12);
				vaultContentLabel.setLayoutData(vaultContentLabelLData);
				vaultContentLabel.setText("Vault Contents");
				vaultContentLabel.setBackground(SWTResourceManager.getColor(255,238,219));
				vaultContentLabel.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				FormData vaultListLData = new FormData();
				vaultListLData.width = 188;
				vaultListLData.height = 233;
				vaultListLData.left =  new FormAttachment(0, 1000, 6);
				vaultListLData.top =  new FormAttachment(0, 1000, 38);
				vaultList = new List(dialogShell, SWT.NONE);
				vaultList.setLayoutData(vaultListLData);
				vaultList.setFont(SWTResourceManager.getFont("Segoe UI", 9, 3, false, false));
				vaultList.setToolTipText("This lists all the campaigns currently stored in the vault");
				vaultList.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						vaultEntryCommentView.setText(Globals.vaultContents.get(vaultList.getSelectionIndex()).getLogFile());
					}
				});
			}
			dialogShell.layout();
			dialogShell.pack();			
			dialogShell.setSize(539, 324);
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));
			dialogShell.setLocation(getParent().toDisplay(0, 0));

			if (Globals.vaultContents.size()==0) {
				Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION |SWT.OK);
				Globals.mBox.setMessage("The Vault is empty. You need to add a campaign to the vault before you can use the vault viewer.");
				Globals.mBox.open();
				//dialogShell.dispose();
			} else {
				for (int x=0;x<Globals.vaultContents.size();x++) {
					vaultList.add(Globals.vaultContents.get(x).getCampaignName());
				}
				dialogShell.open();
			}
			Display display = dialogShell.getDisplay();
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
				display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
