package com.politespider;
import com.cloudgarden.resource.SWTResourceManager;


import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;




public class DLG_CampaignSetup extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private Label campaignNameLabel;
	private Button doneButton;
	private StyledText campaignCommentText;
	private Text campaignNameText;
	private StyledText enterCampaignDescriptionText;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_CampaignSetup inst = new DLG_CampaignSetup(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_CampaignSetup(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			
			
			dialogShell.setLayout(new FormLayout());
			dialogShell.layout();
			dialogShell.pack();	
			dialogShell.setSize(300, 352);
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));
			{
				FormData enterCampaignDescriptionTextLData = new FormData();
				enterCampaignDescriptionTextLData.width = 239;
				enterCampaignDescriptionTextLData.height = 62;
				enterCampaignDescriptionTextLData.left =  new FormAttachment(0, 1000, 21);
				enterCampaignDescriptionTextLData.top =  new FormAttachment(0, 1000, 51);
				enterCampaignDescriptionText = new StyledText(dialogShell, SWT.READ_ONLY | SWT.WRAP);
				enterCampaignDescriptionText.setLayoutData(enterCampaignDescriptionTextLData);
				enterCampaignDescriptionText.setText("Enter a description of your campaign. This will used as the first entry in your pilots log file.");
				enterCampaignDescriptionText.setBackground(SWTResourceManager.getColor(255,238,219));
				enterCampaignDescriptionText.setEnabled(false);
			}
			{
				FormData text1LData = new FormData();
				text1LData.width = 227;
				text1LData.height = 15;
				text1LData.left =  new FormAttachment(0, 1000, 21);
				text1LData.top =  new FormAttachment(0, 1000, 30);
				campaignNameText = new Text(dialogShell, SWT.NONE);
				campaignNameText.setLayoutData(text1LData);
				campaignNameText.setTextLimit(30);
				campaignNameText.setFont(SWTResourceManager.getFont("Segoe UI", 9, 3, false, false));
			}
			{
				FormData styledText1LData = new FormData();
				styledText1LData.width = 232;
				styledText1LData.height = 164;
				styledText1LData.left =  new FormAttachment(0, 1000, 21);
				styledText1LData.top =  new FormAttachment(0, 1000, 113);
				campaignCommentText = new StyledText(dialogShell, SWT.V_SCROLL);
				campaignCommentText.setLayoutData(styledText1LData);
				campaignCommentText.setWordWrap(true);
				campaignCommentText.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				doneButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData DoneButtonLData = new FormData();
				DoneButtonLData.width = 58;
				DoneButtonLData.height = 25;
				DoneButtonLData.left =  new FormAttachment(0, 1000, 109);
				DoneButtonLData.top =  new FormAttachment(0, 1000, 283);
				doneButton.setLayoutData(DoneButtonLData);
				doneButton.setText("Done");
				doneButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						if (campaignNameText.getText().length() ==0) {
							MessageBox messageBox = new MessageBox(dialogShell, SWT.ICON_ERROR
							| SWT.OK);
							messageBox.setMessage("You need to provide a name for your Campaign.");
							messageBox.open();
						} else {
							if(FileHandling.doesFolderExist(Globals.campaignfolderNAME+FileHandling.filenameCleaner(campaignNameText.getText()))) {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION|SWT.OK);
								Globals.mBox.setMessage("That campaign name that is already in use. Please try again.");
								Globals.mBox.open();
							} else {
								// gather the information that will be used to create the new Campaign Info file
								Globals.campaignNameStorage = FileHandling.filenameCleaner(campaignNameText.getText());
								Globals.commentStorage = campaignCommentText.getText();
								dialogShell.dispose();
							}

						}
					}
				});

			}
			{
				campaignNameLabel = new Label(dialogShell, SWT.NONE);
				FormData label2LData = new FormData();
				label2LData.width = 251;
				label2LData.height = 15;
				label2LData.left =  new FormAttachment(0, 1000, 21);
				label2LData.top =  new FormAttachment(0, 1000, 9);
				campaignNameLabel.setLayoutData(label2LData);
				campaignNameLabel.setText("Choose a Campaign Name (30 char max)");
				campaignNameLabel.setBackground(SWTResourceManager.getColor(255,238,219));
				campaignNameLabel.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			dialogShell.setLocation(getParent().toDisplay(100, 0));
			dialogShell.open();
			Display display = dialogShell.getDisplay();
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
				display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
