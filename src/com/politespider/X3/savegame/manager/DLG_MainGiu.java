package com.politespider.X3.savegame.manager;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import java.awt.Desktop;
import com.cloudgarden.resource.SWTResourceManager;



public class DLG_MainGiu extends org.eclipse.swt.widgets.Composite {
	private Menu menu1;
	private Menu menu2;
	private MenuItem FAQItem;
	private MenuItem forumPostItem;
	private MenuItem webLinksItem;
	private Button updateButton;
	private Button deleteButton;
	private Button renameButton;
	private List campaignsList;
	private Label progressLabel;
	private ProgressBar progressBar;
	private Label allAvailable;
	private Text cActiveName;
	private Label cActive;
	private Text cTitle;
	private Button exitButton;
	private Label cInfo;
	private StyledText commentText;
	private Button editComment;
	private Button newCampaign;
	private Button forkButton;
	private Button activateCampaignButton;
	private Button importExportCampaignButton;
	private Menu aboutMenu;
	private MenuItem aboutMenuItem;
	private MenuItem exitMenuItem;
	private Menu fileMenu;
	private MenuItem fileMenuItem;
	private Button vaultButton;
	private MenuItem mainXuForum;

	{
		//Register as a resource user - SWTResourceManager will
		//handle the obtaining and disposing of resources
		SWTResourceManager.registerResourceUser(this);
	}

	public DLG_MainGiu(Composite parent, int style) {
		super(parent, style);
		initGUI();
	}
	
	/**
	* Initialize the GUI.
	*/
	private void initGUI() {
		try {
			this.setBackground(SWTResourceManager.getColor(255,238,219));
			FormLayout thisLayout = new FormLayout();
			this.setLayout(thisLayout);
			{
				vaultButton = new Button(this, SWT.PUSH | SWT.CENTER);
				FormData vaultButtonLData = new FormData();
				vaultButtonLData.width = 175;
				vaultButtonLData.height = 25;
				vaultButtonLData.left =  new FormAttachment(0, 1000, 496);
				vaultButtonLData.top =  new FormAttachment(0, 1000, 176);
				vaultButton.setLayoutData(vaultButtonLData);
				vaultButton.setText("Vault Manager");
				vaultButton.setToolTipText("The Vault stores copies of campaigns that can be used to  create new campaigns without altering the original savegame files");
				vaultButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						// update the stored version of the active campaign first
						File[] tmpFileList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
						if(tmpFileList.length == 1) {
							Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
							Globals.mBox.setMessage("Savegame folder appears to have no save files in it.\n Please check this.");
							Globals.mBox.open();
						} else {
							exitButton.setEnabled(false);
							editComment.setEnabled(false);
							progressBar.setMinimum(0);
							progressBar.setMaximum(tmpFileList.length);
							for (int i=0;i<tmpFileList.length;i++) {
								progressLabel.setText("Updating stored copy of active campaign: "+tmpFileList[i].getName());
								try {
									FileHandling.copySingleFile(tmpFileList[i].toString(),Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName()+"/"+tmpFileList[i].getName().toString());
								} catch (Exception e1) {
									Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
									Globals.mBox.open();
									System.exit(0);
								}
								progressBar.setSelection(i+1);
								progressLabel.setText("Stored campaign updated, Vault Manager activated.");
								progressBar.setSelection(0);
								exitButton.setEnabled(true);
								editComment.setEnabled(true);
							}
						}
						DLG_VaultManager vBox = new DLG_VaultManager(getShell(),1);
						vBox.open();
						// repopulate the campaigns list
						Globals.allCampaigns.clear();
						String[] rList = FileHandling.listDir(Globals.campaignfolderNAME);
						for (int q=0;q<rList.length;q++) {
							Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(Globals.campaignfolderNAME+rList[q]+"/"+Globals.campaignFileName));
						}
						campaignsList.removeAll();
						for (int r=0;r<rList.length;r++) {
							campaignsList.add(Globals.allCampaigns.get(r).getCampaignName());
							if(Globals.allCampaigns.get(r).getCampaignName().equals(Globals.activeCampaign.getCampaignName())) {
								Globals.currentCampaignIndex = r;
								Globals.selectedCampaignIndex = r;
							}
						}

					}

				});

			}
			{
				updateButton = new Button(this, SWT.PUSH | SWT.CENTER);
				FormData updateButtonLData = new FormData();
				updateButtonLData.width = 175;
				updateButtonLData.height = 25;
				updateButtonLData.left =  new FormAttachment(0, 1000, 496);
				updateButtonLData.top =  new FormAttachment(0, 1000, 19);
				updateButton.setLayoutData(updateButtonLData);
				updateButton.setText("Save Active Campaign");
				updateButton.setToolTipText("Sends a copy of the active game into its storage folder, updating the stored copy with your progress");
				updateButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) { 
						// first test whether the active campaign already has a storage folder, or whether
						// If it doesn't, it may be from a previous install, or another campaign set, and therefore needs to be handled differently.
						if (CampaignUtils.isActiveCampaignAlreadyStored()) {
							File[] tmpFileList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
							if(tmpFileList.length == 1) {
								Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
								Globals.mBox.setMessage("Savegame folder appears to have no save files in it.\n Please check this.");
								Globals.mBox.open();
							} else {
								exitButton.setEnabled(false);
								editComment.setEnabled(false);
								progressBar.setMinimum(0);
								progressBar.setMaximum(tmpFileList.length);
								for (int i=0;i<tmpFileList.length;i++) {
									progressLabel.setText("Updating stored copy of active campaign: "+tmpFileList[i].getName());
									try {
										FileHandling.copySingleFile(tmpFileList[i].toString(),Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName()+"/"+tmpFileList[i].getName().toString());
									} catch (Exception e1) {
										Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
										Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
										Globals.mBox.open();
										System.exit(0);
									}
									progressBar.setSelection(i+1);
									progressLabel.setText("Stored campaign updated.");
									progressBar.setSelection(0);
									exitButton.setEnabled(true);
									editComment.setEnabled(true);
								}
							}

						} else { // The active campaign isn't already held in storage, some user decision making is required
							Globals.mBox = new MessageBox(getShell(), SWT.ICON_QUESTION |SWT.YES | SWT.NO);
							Globals.mBox.setMessage("The active campaign has not been stored yet.\n\n Would you like it to be stored in you campaigns folder?");
							int response = Globals.mBox.open();
							if (response == SWT.YES) {
								FileHandling.createFolder(Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName());
								exitButton.setEnabled(false);
								editComment.setEnabled(false);
								File[] tmpFileList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
								if(tmpFileList.length == 1) {
									Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("The X3 Savegame folder appears to have no save files in it.\n Please check this.");
									Globals.mBox.open();
								} else {
									progressBar.setMinimum(0);
									progressBar.setMaximum(tmpFileList.length);
									for (int i=0;i<tmpFileList.length;i++) {
										progressLabel.setText("Storing the active campaign in your Campaigns folder: "+tmpFileList[i].getName());
										try {
											FileHandling.copySingleFile(tmpFileList[i].toString(),Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName()+"/"+tmpFileList[i].getName().toString());
										} catch (Exception e1) {
											Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
											Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
											Globals.mBox.open();
											System.exit(0);
										}
										progressBar.setSelection(i);
										progressLabel.setText("Active campaign is now stored.");
										progressBar.setSelection(0);
										String fd = new String(Globals.campaignfolderNAME);
										String[] files = FileHandling.listDir(fd);
										Globals.allCampaigns.clear();
										campaignsList.removeAll();
										for ( int d = 0; d < files.length; d++ ) {
											Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(fd+files[d]+"/"+Globals.campaignFileName));
											campaignsList.add(Globals.allCampaigns.get(d).getCampaignName());
										}
										Globals.selectedCampaignIndex = 0;            						
										if (Globals.selectedCampaignIndex == Globals.currentCampaignIndex) {
											cTitle.setText(Globals.activeCampaign.getCampaignName());
											commentText.setText(Globals.activeCampaign.getLogFile());

										} else {
											cTitle.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName());
											commentText.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getLogFile());
										}
										exitButton.setEnabled(true);
										editComment.setEnabled(true);
									}
								}
							}
							if (response == SWT.NO) {
								System.exit(0);
							}
						}
					}}); 
			}
			{
				FormData campaignsListLData = new FormData();
				campaignsListLData.width = 210;
				campaignsListLData.height = 188;
				campaignsListLData.left =  new FormAttachment(0, 1000, 4);
				campaignsListLData.top =  new FormAttachment(0, 1000, 68);
				campaignsList = new List(this, SWT.V_SCROLL | SWT.BORDER);
				campaignsList.setLayoutData(campaignsListLData);
				campaignsList.setFont(SWTResourceManager.getFont("Segoe UI", 9, 3, false, false));
				campaignsList.setToolTipText("All the campaigns you currently have are listed here");
				campaignsList.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent arg0) {
						activateCampaignButton.setEnabled(true);
						// first test whether the active campaign already has a storage folder, or whether
						// If it doesn't, it may be from a previous install, or another campaign set, and therefore needs to be handled differently.
						if (CampaignUtils.isActiveCampaignAlreadyStored()) {
							if (campaignsList.getSelectionIndex() !=-1) {
								Globals.selectedCampaignIndex = campaignsList.getSelectionIndex();
								if (Globals.selectedCampaignIndex == Globals.currentCampaignIndex) {
									cTitle.setText(Globals.activeCampaign.getCampaignName());
									commentText.setText(Globals.activeCampaign.getLogFile());
								} else {
									cTitle.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName());
									commentText.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getLogFile());
								}
							}
						} else { // The active campaign isn't already held in storage, some user decision making is required
							Globals.mBox = new MessageBox(getShell(), SWT.ICON_QUESTION |SWT.YES | SWT.NO);
							Globals.mBox.setMessage("The active campaign has not been stored yet.\n\n Would you like it to be stored in you campaigns folder?");
							int response = Globals.mBox.open();
							if (response == SWT.YES) {
								FileHandling.createFolder(Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName());
								File[] tmpFileList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
								if(tmpFileList.length == 1) {
									Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("Savegame folder appears to have no save files in it.\n Please check this.");
									Globals.mBox.open();
								} else {
									exitButton.setEnabled(false);
									editComment.setEnabled(false);
									progressBar.setMinimum(0);
									progressBar.setMaximum(tmpFileList.length);
									for (int i=0;i<tmpFileList.length;i++) {
										progressLabel.setText("Storing the active campaign in your Campaigns folder: "+tmpFileList[i].getName());
										try {
											FileHandling.copySingleFile(tmpFileList[i].toString(),Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName()+"/"+tmpFileList[i].getName().toString());
										} catch (Exception e1) {
											Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
											Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
											Globals.mBox.open();
											System.exit(0);
										}
										progressBar.setSelection(i);
										progressLabel.setText("Active campaign is now stored.");
										progressBar.setSelection(0);
										String fd = new String(Globals.campaignfolderNAME);
										String[] files = FileHandling.listDir(fd);
										Globals.allCampaigns.clear();
										campaignsList.removeAll();
										for ( int d = 0; d < files.length; d++ ) {
											Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(fd+files[d]+"/"+Globals.campaignFileName));
											campaignsList.add(Globals.allCampaigns.get(d).getCampaignName());
										}
										Globals.selectedCampaignIndex = 0;            						
										if (Globals.selectedCampaignIndex == Globals.currentCampaignIndex) {
											cTitle.setText(Globals.activeCampaign.getCampaignName());
											commentText.setText(Globals.activeCampaign.getLogFile());
										} else {
											cTitle.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName());
											commentText.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getLogFile());

										}
										exitButton.setEnabled(true);
										editComment.setEnabled(true);
									}
								}
								
							}
							if (response == SWT.NO) {
								System.exit(0);
							}
						}
					}
				});
			}
			{
				allAvailable = new Label(this, SWT.NONE);
				FormData allAvailableLData = new FormData();
				allAvailableLData.width = 142;
				allAvailableLData.height = 15;
				allAvailableLData.left =  new FormAttachment(0, 1000, 47);
				allAvailableLData.top =  new FormAttachment(0, 1000, 50);
				allAvailable.setLayoutData(allAvailableLData);
				allAvailable.setText("All Available Campaigns");
				allAvailable.setBackground(SWTResourceManager.getColor(255,238,219));
				allAvailable.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				FormData cActiveNameLData = new FormData();
				cActiveNameLData.width = 193;
				cActiveNameLData.height = 15;
				cActiveNameLData.left =  new FormAttachment(0, 1000, 12);
				cActiveNameLData.top =  new FormAttachment(0, 1000, 21);
				cActiveName = new Text(this, SWT.BORDER);
				cActiveName.setLayoutData(cActiveNameLData);
				cActiveName.setTextLimit(30);
				cActiveName.setFont(SWTResourceManager.getFont("Segoe UI", 9, 3, false, false));
				cActiveName.setEditable(false);
				cActiveName.setToolTipText("This campaign is the one that is currently in the X3 savegame folder");
			}
			{
				cActive = new Label(this, SWT.NONE);
				FormData cActiveLData = new FormData();
				cActiveLData.width = 207;
				cActiveLData.height = 15;
				cActiveLData.left =  new FormAttachment(0, 1000, 12);
				cActiveLData.top =  new FormAttachment(0, 1000, 3);
				cActive.setLayoutData(cActiveLData);
				cActive.setText("Currently Active Campaign");
				cActive.setBackground(SWTResourceManager.getColor(255,238,219));
				cActive.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				cTitle = new Text(this, SWT.BORDER);
				FormData cTitleLData = new FormData();
				cTitleLData.width = 188;
				cTitleLData.height = 16;
				cTitleLData.left =  new FormAttachment(0, 1000, 261);
				cTitleLData.top =  new FormAttachment(0, 1000, 21);
				cTitle.setLayoutData(cTitleLData);
				cTitle.setFont(SWTResourceManager.getFont("Segoe UI", 9, 3, false, false));
				cTitle.setEditable(false);
				cTitle.setToolTipText("This is the campaign currently highlighted in the list of available campaigns");
			}
			{
				exitButton = new Button(this, SWT.PUSH | SWT.CENTER);
				FormData exitButtonLData = new FormData();
				exitButtonLData.width = 50;
				exitButtonLData.height = 25;
				exitButtonLData.left =  new FormAttachment(0, 1000, 629);
				exitButtonLData.top =  new FormAttachment(0, 1000, 280);
				exitButton.setLayoutData(exitButtonLData);
				exitButton.setText("Exit");
				exitButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) { 
						System.exit(0);
					} 
				}); 
			}
			{
				cInfo = new Label(this, SWT.NONE);
				FormData cInfoLData = new FormData();
				cInfoLData.width = 216;
				cInfoLData.height = 15;
				cInfoLData.left =  new FormAttachment(0, 1000, 261);
				cInfoLData.top =  new FormAttachment(0, 1000, 3);
				cInfo.setLayoutData(cInfoLData);
				cInfo.setText("Selected Campaign Information");
				cInfo.setBackground(SWTResourceManager.getColor(255,238,219));
				cInfo.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				commentText = new StyledText(this, SWT.NO_FOCUS | SWT.V_SCROLL | SWT.BORDER);
				FormData commentTextLData = new FormData();
				commentTextLData.width = 197;
				commentTextLData.height = 189;
				commentTextLData.left =  new FormAttachment(0, 1000, 259);
				commentTextLData.top =  new FormAttachment(0, 1000, 69);
				commentText.setLayoutData(commentTextLData);
				commentText.setWordWrap(true);
				commentText.setEditable(false);
				commentText.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
				commentText.setToolTipText("The most recent pilots log entry");
				
			}
			{
				editComment = new Button(this, SWT.PUSH | SWT.CENTER);
				FormData editCommentLData = new FormData();
				editCommentLData.width = 221;
				editCommentLData.height = 21;
				editCommentLData.left =  new FormAttachment(0, 1000, 259);
				editCommentLData.top =  new FormAttachment(0, 1000, 46);
				editComment.setLayoutData(editCommentLData);
				editComment.setText("Open the Pilots log file editor");
				editComment.setToolTipText("View the logfile in a larger window and add new entries");
				editComment.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						DLG_PilotsLogEditor eBox = new DLG_PilotsLogEditor(getShell(),1);
						eBox.open();
						CampaignFileHandling.newCampaignFile(Globals.saveFolder+"/"+Globals.campaignFileName, Globals.activeCampaign.getCampaignName(), Globals.activeCampaign.getLogFile(),Globals.activeCampaign.getLogEntryNumber(),Globals.activeCampaign.getLogEntryTemplate());
						commentText.setText(Globals.activeCampaign.getLogFile());
					}
				});
			}
			{
				newCampaign = new Button(this, SWT.PUSH | SWT.CENTER);
				FormData newCampaignLData = new FormData();
				newCampaignLData.width = 175;
				newCampaignLData.height = 25;
				newCampaignLData.left =  new FormAttachment(0, 1000, 496);
				newCampaignLData.top =  new FormAttachment(0, 1000, 114);
				newCampaign.setLayoutData(newCampaignLData);
				newCampaign.setText("Begin New Campaign");
				newCampaign.setToolTipText("This will move the current active campaign back into storage and empty the savegame folder, so you can begin your new campaign");
				newCampaign.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) { 
						// first test whether the active campaign already has a storage folder, or whether
						// If it doesn't, it may be from a previous install, or another campaign set, and therefore needs to be handled differently.
						if (CampaignUtils.isActiveCampaignAlreadyStored()) {
							Globals.mBox = new MessageBox(getShell(), SWT.ICON_QUESTION |SWT.YES | SWT.NO);
							Globals.mBox.setMessage("If you select 'YES' your current savegame set will be returned to \nstorage, and the savegame folder will be cleared, aside from the campaign file you will now specify.");
							int response = Globals.mBox.open();
							if (response == SWT.YES) {
								// null the fields we will need to be checking later to ensure data was entered
								Globals.campaignNameStorage = null;
								Globals.commentStorage = null;
								DLG_CampaignSetup newGameBox = new DLG_CampaignSetup(getShell(),SWT.APPLICATION_MODAL);
								newGameBox.open();
								//check to see that we have a campaign name specified, if not, exit (null or empty)
								if (Globals.campaignNameStorage ==null) {
									Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("Campaign name not properly set - aborting operation");
									Globals.mBox.open();
									progressLabel.setText("New campaign creation aborted.");
								}
								else {
									if(Globals.campaignNameStorage.length()==0) {
										Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
										Globals.mBox.setMessage("Campaign name empty after invalid filename character stripping - aborting operation");
										Globals.mBox.open();
										progressLabel.setText("New campaign creation aborted.");
									} else {
										// ok we got this far, the information to create a campaign file exists
										Globals.newCampaign = new CampaignInfo();
										Globals.newCampaign.setCampaignName(Globals.campaignNameStorage);
			   							if (FileHandling.isFilenameAlreadyInUse(Globals.newCampaign.getCampaignName())) {
											Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
											Globals.mBox.setMessage("Campaign name already in use - aborting operation");
											Globals.mBox.open();
											progressLabel.setText("New campaign creation aborted.");
										} else {
											// move the current campaign back to storage
											exitButton.setEnabled(false);
											editComment.setEnabled(false);
											File[] tmpFileList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
											if(tmpFileList.length == 1) {
												Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
												Globals.mBox.setMessage("Savegame folder appears to have no save files in it.\n Please check this.");
												Globals.mBox.open();
												System.exit(0);
											}
											progressBar.setMinimum(0);
											progressBar.setMaximum(tmpFileList.length);
											for (int i=0;i<tmpFileList.length;i++) {
												progressLabel.setText("Copying savegame files to the Campaign storage folder: "+tmpFileList[i].getName());
												try {
													FileHandling.copySingleFile(tmpFileList[i].toString(),Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName()+"/"+tmpFileList[i].getName().toString());
												} catch (Exception e1) {
													Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
													Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
													Globals.mBox.open();
													System.exit(0);
												}
												progressBar.setSelection(i);
											}
											progressLabel.setText("");
											progressBar.setSelection(0);
											exitButton.setEnabled(true);
											editComment.setEnabled(true);
											FileHandling.emptyDirectory(Globals.saveFolder);    							
											
											// make the folder that will house this campaign and its restore points
											FileHandling.createFolder(Globals.campaignfolderNAME+Globals.newCampaign.getCampaignName());

											// create the new Campaign Info file
											try {
												CampaignFileHandling.newCampaignFile(Globals.saveFolder+"campaign file.xml", FileHandling.filenameCleaner(Globals.newCampaign.getCampaignName()), Globals.newCampaign.getLogFile(),1,Globals.defaultLogEntry);
												CampaignFileHandling.newCampaignFile(Globals.campaignfolderNAME+Globals.newCampaign.getCampaignName()+"/campaign file.xml", FileHandling.filenameCleaner(Globals.newCampaign.getCampaignName()), Globals.newCampaign.getLogFile(),1,Globals.defaultLogEntry);
											} catch (Exception e2) {
												Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
												Globals.mBox.setMessage("Unable to create new campaign file. \nNew campaign operation aborted - no new campaign has been created.");
												Globals.mBox.open();
												System.exit(0);
											}
											Globals.mBox = new MessageBox(getShell(), SWT.ICON_INFORMATION
											| SWT.OK);
											Globals.mBox.setMessage("This application will now exit, launch X3 now and start your new campaign. \nThe next time you start the manager, the new campaign will be activated.\n You will need to click the 'Update Active Campaign' button when you have some savegames\n in order to make sure the Savegame Manager has copies of them.");
											Globals.mBox.setText("New Campaign");
											Globals.mBox.open();
											System.exit(0);	
										}
									}
								}
							}
						} else { // The active campaign isn't already held in storage, some user decision making is required
							Globals.mBox = new MessageBox(getShell(), SWT.ICON_QUESTION |SWT.YES | SWT.NO);
							Globals.mBox.setMessage("The active campaign has not been stored yet.\n\n Would you like it to be stored in you campaigns folder?\n\n Answering 'YES' will cause a storage folder to be created, and the active campaign will be added to your current Campaigns folder.\n\n Answering NO will exit this program so you can resolve the issue yourself. \n\nDeleting the 'campaign info.xml' file in \n"+Globals.saveFolder+"\nand restarting the Savegame Manager will let you add the campaign under a new name if you wish.");
							int response = Globals.mBox.open();
							if (response == SWT.YES) {
								FileHandling.createFolder(Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName());
								exitButton.setEnabled(false);
								editComment.setEnabled(false);
								File[] tmpFileList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
								if(tmpFileList.length == 1) {
									Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("Savegame folder appears to have no save files in it.\n Please check this.");
									Globals.mBox.open();
									System.exit(0);
								}
								progressBar.setMinimum(0);
								progressBar.setMaximum(tmpFileList.length);
								for (int i=0;i<tmpFileList.length;i++) {
									progressLabel.setText("Storing the active campaign in your Campaigns folder: "+tmpFileList[i].getName());
									try {
										FileHandling.copySingleFile(tmpFileList[i].toString(),Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName()+"/"+tmpFileList[i].getName().toString());
									} catch (Exception e1) {
										Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
										Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
										Globals.mBox.open();
										System.exit(0);
									}
									progressBar.setSelection(i);
									progressLabel.setText("Active campaign is now stored.");
									progressBar.setSelection(0);
									String fd = new String(Globals.campaignfolderNAME);
									String[] files = FileHandling.listDir(fd);
									Globals.allCampaigns.clear();
									campaignsList.removeAll();
									for ( int d = 0; d < files.length; d++ ) {
										Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(fd+files[d]+"/"+Globals.campaignFileName));
										campaignsList.add(Globals.allCampaigns.get(d).getCampaignName());
									}
									Globals.selectedCampaignIndex = 0;            						
									if (Globals.selectedCampaignIndex == Globals.currentCampaignIndex) {
										cTitle.setText(Globals.activeCampaign.getCampaignName());
										commentText.setText(Globals.activeCampaign.getLogFile());
									} else {
										cTitle.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName());
										commentText.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getLogFile());
									}
									exitButton.setEnabled(true);
									editComment.setEnabled(true);
								}
							}
							if (response == SWT.NO) {
								System.exit(0);
							}
						}
					} 
				}); 
			}
			{
				activateCampaignButton = new Button(this, SWT.PUSH | SWT.CENTER);
				FormData activateCampaignButtonLData = new FormData();
				activateCampaignButtonLData.width = 175;
				activateCampaignButtonLData.height = 25;
				activateCampaignButtonLData.left =  new FormAttachment(0, 1000, 496);
				activateCampaignButtonLData.top =  new FormAttachment(0, 1000, 50);
				activateCampaignButton.setLayoutData(activateCampaignButtonLData);
				activateCampaignButton.setText("Activate Selected Campaign");
				activateCampaignButton.setToolTipText("This will make the currently selected campaign the active one (its savegames will be in the X3 savegame folder)");
				activateCampaignButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						// first test whether the active campaign already has a storage folder, or whether
						// If it doesn't, it may be from a previous install, or another campaign set, and therefore needs to be handled differently.
						if (CampaignUtils.isActiveCampaignAlreadyStored()) {
							// find out which campaign is to be brought from storage
							int index = campaignsList.getSelectionIndex();
							if (index==-1) {
								index =0;
							}
							if (Globals.allCampaigns.get(index).getCampaignName().equals(Globals.activeCampaign.getCampaignName())) {
								Globals.mBox = new MessageBox(getShell(), SWT.ICON_INFORMATION
								| SWT.OK);
								Globals.mBox.setMessage("The selected Campaign is already the active Campaign");
								Globals.mBox.setText("Campaign Selection");
								Globals.mBox.open();
							} else {
								// before we get started, we need to make sure the campaign we are trying to switch to actually contains savegame files
								File[] tmpFileList3 = FileHandling.listDirectoryContent(Globals.campaignfolderNAME+Globals.allCampaigns.get(index).getCampaignName(),Globals.campaignFileExtensions,false);
								if((tmpFileList3.length == 0)||(tmpFileList3.length == 1)) {
									Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("The selected campaign appears to have no savegame files in its storage folder.\n Please check this.\n As this is an issue that may cause problems for the Savegame Manager, it will now exit");
									Globals.mBox.open();
									System.exit(0);
								}
								// move the current campaign back to storage
								exitButton.setEnabled(false);
								editComment.setEnabled(false);
								File[] tmpFileList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
								if((tmpFileList.length == 0)||(tmpFileList.length == 1)) {
									Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("The currently active campaign appears to have no savegame files in the X3TC savegame folder.\n Please check this.  The Savegame Manager will now exit, as it needs to go and have a lie down");
									Globals.mBox.open();
									System.exit(0);
								}
								progressBar.setMinimum(0);
								progressBar.setMaximum(tmpFileList.length);
								for (int i=0;i<tmpFileList.length;i++) {
									progressLabel.setText("Returning the previously active campaign to storage: "+tmpFileList[i].getName());
									try {
										FileHandling.copySingleFile(tmpFileList[i].toString(),Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName()+"/"+tmpFileList[i].getName().toString());
									} catch (Exception e1) {
										Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
										Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
										Globals.mBox.open();
										System.exit(0);
									}
									progressBar.setSelection(i);
								}
								progressLabel.setText("");
								progressBar.setSelection(0);
								// now clear the savegame folder
								FileHandling.emptyDirectory(Globals.saveFolder);
								// and move the newly activated campaign into the save folder.
								progressBar.setMinimum(0);
								progressBar.setMaximum(tmpFileList3.length);
								for (int i=0;i<tmpFileList3.length;i++) {
									progressLabel.setText("Activating the selected campaign: "+tmpFileList3[i].getName());
									try {
										FileHandling.copySingleFile(tmpFileList3[i].toString(),Globals.saveFolder+"/"+tmpFileList3[i].getName().toString());
									} catch (Exception e1) {
										Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
										Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList3[i].toString());
										Globals.mBox.open();
										System.exit(0);
									}
									progressBar.setSelection(i);
								}
								progressLabel.setText("Active Campaign Change Completed");	
								progressBar.setSelection(0);
								exitButton.setEnabled(true);
								editComment.setEnabled(true);
								// as we just changed the content of a campaign file, we need to reload the campaigns list
								Globals.allCampaigns.clear();
								String[] cList = FileHandling.listDir(Globals.campaignfolderNAME);
								for (int q=0;q<cList.length;q++) {
									Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(Globals.campaignfolderNAME+cList[q]+"/"+Globals.campaignFileName));
								}
								Globals.activeCampaign.setCampaignName(Globals.allCampaigns.get(campaignsList.getSelectionIndex()).getCampaignName());
								Globals.activeCampaign.setLogFile(Globals.allCampaigns.get(campaignsList.getSelectionIndex()).getLogFile());
								Globals.activeCampaign.setLogEntryTemplate(Globals.allCampaigns.get(campaignsList.getSelectionIndex()).getLogEntryTemplate());
								Globals.activeCampaign.setLogEntryNumber(Globals.allCampaigns.get(campaignsList.getSelectionIndex()).getLogEntryNumber());
								Globals.currentCampaignIndex = campaignsList.getSelectionIndex();
								cActiveName.setText(Globals.activeCampaign.getCampaignName());
								commentText.setText(Globals.activeCampaign.getLogFile());
								cTitle.setText(Globals.activeCampaign.getCampaignName());
								// write the campaign info file back, just in case the program has updated the file to the latest format
								CampaignFileHandling.newCampaignFile(Globals.saveFolder+Globals.campaignFileName,Globals.activeCampaign.getCampaignName(), Globals.activeCampaign.getLogFile(),Globals.activeCampaign.getLogEntryNumber(),Globals.activeCampaign.getLogEntryTemplate());						
							}
						} else {
							// The active campaign isn't already held in storage, some user decision making is required
							Globals.mBox = new MessageBox(getShell(), SWT.ICON_QUESTION |SWT.YES | SWT.NO);
							Globals.mBox.setMessage("The active campaign has not been stored yet.\n\n Would you like it to be stored in you campaigns folder?\n\n Answering 'YES' will cause a storage folder to be created, and the active campaign will be added to your current Campaigns folder.\n\n Answering NO will exit this program so you can resolve the issue yourself. \n\nDeleting the 'campaign info.xml' file in \n"+Globals.saveFolder+"\nand restarting the Savegame Manager will let you add the campaign under a new name if you wish.");
							int response = Globals.mBox.open();
							if (response == SWT.YES) {
								FileHandling.createFolder(Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName());
								exitButton.setEnabled(false);
								editComment.setEnabled(false);
								File[] tmpFileList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
								if((tmpFileList.length == 0)||(tmpFileList.length == 1)) {
									Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("Savegame folder appears to have no save files in it.\n Please check this.");
									Globals.mBox.open();
									System.exit(0);
								}
								progressBar.setMinimum(0);
								progressBar.setMaximum(tmpFileList.length);
								for (int i=0;i<tmpFileList.length;i++) {
									progressLabel.setText("Storing the active campaign in your campaigns folder: "+tmpFileList[i].toString());
									try {
										FileHandling.copySingleFile(tmpFileList[i].toString(),Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName()+"/"+tmpFileList[i].getName().toString());
									} catch (Exception e1) {
										Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
										Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
										Globals.mBox.open();
										System.exit(0);
									}
									progressBar.setSelection(i);
									progressLabel.setText("Active campaign is now stored.");
									progressBar.setSelection(0);
									String fd = new String(Globals.campaignfolderNAME);
									String[] files = FileHandling.listDir(fd);
									Globals.allCampaigns.clear();
									campaignsList.removeAll();
									for ( int d = 0; d < files.length; d++ ) {
										Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(fd+files[d]+"/"+Globals.campaignFileName));
										campaignsList.add(Globals.allCampaigns.get(d).getCampaignName());
									}
									Globals.selectedCampaignIndex = 0;            						
									if (Globals.selectedCampaignIndex == Globals.currentCampaignIndex) {
										cTitle.setText(Globals.activeCampaign.getCampaignName());
										commentText.setText(Globals.activeCampaign.getLogFile());
									} else {
										cTitle.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName());
										commentText.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getLogFile());
									}
									exitButton.setEnabled(true);
									editComment.setEnabled(true);
								}
							}
							if (response == SWT.NO) {
								System.exit(0);
							}
						}
					}
				}); 
			}
			{
				forkButton = new Button(this, SWT.PUSH | SWT.CENTER);
				FormData forkButtonLData = new FormData();
				forkButtonLData.width = 175;
				forkButtonLData.height = 25;
				forkButtonLData.left =  new FormAttachment(0, 1000, 496);
				forkButtonLData.top =  new FormAttachment(0, 1000, 83);
				forkButton.setLayoutData(forkButtonLData);
				forkButton.setText("Fork Active Campaign");
				forkButton.setToolTipText("Make a copy of the selected Campaign, rename it and use it as the basis for a new campaign");
				forkButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) { 
						// first test whether the active campaign already has a storage folder, or whether
						// If it doesn't, it may be from a previous install, or another campaign set, and therefore needs to be handled differently.
						if (CampaignUtils.isActiveCampaignAlreadyStored()) {
							Globals.mBox= new MessageBox(getShell(), SWT.ICON_QUESTION |SWT.YES | SWT.NO);
							Globals.mBox.setMessage("If you select 'YES' the stored version of your current campaign will be updated.\nCopies of the savegames from your active campaign will then be moved to the storage folder for the forked campaign.");
							int response = Globals.mBox.open();
							if (response == SWT.YES) {
								// move the current campaign back to storage
								exitButton.setEnabled(false);
								editComment.setEnabled(false);
								// null the fields we will need to be checking later to ensure data was entered
								Globals.campaignNameStorage = null;
								Globals.commentStorage = null;
								DLG_CampaignSetup forkGameBox = new DLG_CampaignSetup(getShell(),SWT.APPLICATION_MODAL);
								forkGameBox.open();
								//check to see that we have a campaign name specified, if not, exit (null or empty)
								if (Globals.campaignNameStorage ==null) {
									Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("Campaign name not properly set - aborting operation");
									Globals.mBox.open();
									progressLabel.setText(" Campaign Fork Aborted");
									editComment.setEnabled(true);
									exitButton.setEnabled(true);
								} else {
									if(Globals.campaignNameStorage.length()==0) {
										Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
										Globals.mBox.setMessage("Campaign name empty after invalid filename character stripping - aborting operation");
										Globals.mBox.open();
										progressLabel.setText(" Campaign Fork Aborted");
										editComment.setEnabled(true);
										exitButton.setEnabled(true);
									} else {
										// ok we got this far, the information to create a campaign file exists
										Globals.forkedCampaign = new CampaignInfo();
										Globals.forkedCampaign.setCampaignName(Globals.campaignNameStorage);
										Globals.forkedCampaign.setLogFile("");
										Globals.forkedCampaign.setLogEntryNumber(1);
										Globals.forkedCampaign.setLogEntryTemplate(Globals.defaultLogEntry);
										File[] tmpFileList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
										if((tmpFileList.length == 0)||(tmpFileList.length == 1)) {
											Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
											Globals.mBox.setMessage("Savegame folder appears to have no save files in it.\n Please check this.");
											Globals.mBox.open();
											System.exit(0);
										}
										progressBar.setMinimum(0);
										progressBar.setMaximum(tmpFileList.length);
										for (int i=0;i<tmpFileList.length;i++) {
											progressLabel.setText("Updating the stored copy of the active campaign: "+tmpFileList[i].getName());
											try {
												FileHandling.copySingleFile(tmpFileList[i].toString(),Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName()+"/"+tmpFileList[i].getName().toString());
											} catch (Exception e1) {
												Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
												Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
												Globals.mBox.open();
												System.exit(0);
											}
											progressBar.setSelection(i+1);
										}
										progressLabel.setText("");
										progressBar.setSelection(0);

										try {
											// make the folder that will house this campaign
											FileHandling.createFolder(Globals.campaignfolderNAME+Globals.forkedCampaign.getCampaignName());
										} catch (Exception e1) {
											Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
											Globals.mBox.setMessage("Unable to create the storage folder for the specified campaign. ");
											Globals.mBox.open();
											System.exit(0);
										}
										// and copy the files from the save folder into this newly created store.
										File[] tmpFileList2 = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
										if(tmpFileList.length == 1) {
											Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
											Globals.mBox.setMessage("Savegame folder appears to have no save files in it.\n Please check this.");
											Globals.mBox.open();
											System.exit(0);
										}

										progressBar.setMinimum(0);
										progressBar.setMaximum(tmpFileList2.length);
										for (int i=0;i<tmpFileList2.length;i++) {
											progressLabel.setText("Copying savegame files into the new campaigns storage folder "+tmpFileList2[i].getName());
											try {
												FileHandling.copySingleFile(tmpFileList2[i].toString(),Globals.campaignfolderNAME+Globals.forkedCampaign.getCampaignName()+"/"+tmpFileList2[i].getName().toString());
											} catch (Exception e1) {
												Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
												Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
												Globals.mBox.open();
												System.exit(0);
											}
											progressBar.setSelection(i);
										}
										// create the new Campaign Info file (thus overwriting the campaign info file that just got copied over)
										try {
											CampaignFileHandling.newCampaignFile(Globals.campaignfolderNAME+Globals.forkedCampaign.getCampaignName()+"/"+"campaign file.xml", FileHandling.filenameCleaner(Globals.forkedCampaign.getCampaignName()), Globals.forkedCampaign.getLogFile(),Globals.startingLogNumber,Globals.defaultLogEntry);
										} catch (Exception e1) {
											Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
											Globals.mBox.setMessage("Unable to create new campaign file in the campaigns folder, file in use or write access has been restricted. \nFork campaign operation aborted - no new campaign has been created\n please close any other programs that may be using the file and try again. ");
											Globals.mBox.open();
											System.exit(0);
										}
										progressLabel.setText(" Campaign Fork Completed");
										progressBar.setSelection(0);
										exitButton.setEnabled(true);
										editComment.setEnabled(true);
										// reset the campaigns list
										campaignsList.removeAll();
										Globals.allCampaigns.clear();
										Globals.allCampaigns = new ArrayList<CampaignInfo>();
										String fd = new String(Globals.campaignfolderNAME);
										String[] files = FileHandling.listDir(fd);
										for ( int i = 0; i < files.length; i++ ) {
											Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(fd+files[i]+"/"+Globals.campaignFileName));
											campaignsList.add(Globals.allCampaigns.get(i).getCampaignName());
										}
										// we need to set up the active and selected campaign indexes again
										for (int r=0;r<Globals.allCampaigns.size();r++) {
											if (Globals.allCampaigns.get(r).getCampaignName().equals(Globals.activeCampaign.getCampaignName())) {
												Globals.currentCampaignIndex = r;
											}
										}
										Globals.selectedCampaignIndex = Globals.currentCampaignIndex;
										campaignsList.select(Globals.selectedCampaignIndex);
										progressLabel.setText("Campaign Fork Completed");
									}
								}
							}
						} else {
							// The active campaign isn't already held in storage, some user decision making is required
							Globals.mBox = new MessageBox(getShell(), SWT.ICON_QUESTION |SWT.YES | SWT.NO);
							Globals.mBox.setMessage("The active campaign has not been stored yet.\n\n Would you like it to be stored in you campaigns folder?\n\n Answering 'YES' will cause a storage folder to be created, and the active campaign will be added to your current Campaigns folder.\n\n Answering NO will exit this program so you can resolve the issue yourself. \n\nDeleting the 'campaign info.xml' file in \n"+Globals.saveFolder+"\nand restarting the Savegame Manager will let you add the campaign under a new name if you wish.");
							int response = Globals.mBox.open();
							if (response == SWT.YES) {
								FileHandling.createFolder(Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName());
								exitButton.setEnabled(false);
								editComment.setEnabled(false);
								File[] tmpFileList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
								if(tmpFileList.length == 1) {
									Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
									Globals.mBox.setMessage("The X3 Savegame folder appears to contain no savegame files.\n This program will now close. Please check the savegame folder yourself.");
									Globals.mBox.open();
									System.exit(0);
								}
								progressBar.setMinimum(0);
								progressBar.setMaximum(tmpFileList.length);
								for (int i=0;i<tmpFileList.length;i++) {
									progressLabel.setText("Storing the active campaign in your Campaigns folder: "+tmpFileList[i].getName());
									try {
										FileHandling.copySingleFile(tmpFileList[i].toString(),Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName()+"/"+tmpFileList[i].getName().toString());
									} catch (Exception e1) {
										Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
										Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
										Globals.mBox.open();
										System.exit(0);
									}
									progressBar.setSelection(i);
									progressLabel.setText("Active campaign is now stored.");
									progressBar.setSelection(0);
									String[] files = FileHandling.listDir(Globals.campaignfolderNAME);
									Globals.allCampaigns.clear();
									campaignsList.removeAll();
									for ( int d = 0; d < files.length; d++ ) {
										Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(Globals.campaignfolderNAME+files[d]+"/"+Globals.campaignFileName));
										campaignsList.add(Globals.allCampaigns.get(d).getCampaignName());
									}
									Globals.selectedCampaignIndex = 0;            					
									if (Globals.selectedCampaignIndex == Globals.currentCampaignIndex) {
										cTitle.setText(Globals.activeCampaign.getCampaignName());
										commentText.setText(Globals.activeCampaign.getLogFile());

									} else {
										cTitle.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName());
										commentText.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getLogFile());
										
									}
									exitButton.setEnabled(true);
									editComment.setEnabled(true);
								}
							}
							if (response == SWT.NO) {
								System.exit(0);
							}
						}
					} 
				}); 
			}
			{
				importExportCampaignButton = new Button(this, SWT.PUSH | SWT.CENTER);
				importExportCampaignButton.setText("Import/Export Manager");
				FormData importExportCampaignButtonLData = new FormData();
				importExportCampaignButtonLData.width = 175;
				importExportCampaignButtonLData.height = 25;
				importExportCampaignButtonLData.left =  new FormAttachment(0, 1000, 496);
				importExportCampaignButtonLData.top =  new FormAttachment(0, 1000, 145);
				importExportCampaignButton.setLayoutData(importExportCampaignButtonLData);
				importExportCampaignButton.setToolTipText("Import or export campaigns to other locations, such as a usb stick");
				importExportCampaignButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) { 
						DLG_ImportExportManager imexManager = new DLG_ImportExportManager(getShell(), SWT.Activate);
						imexManager.open();
						// reset the campaigns list, so any imported campaign/s show
						campaignsList.removeAll();
						Globals.allCampaigns.clear();
						Globals.allCampaigns = new ArrayList<CampaignInfo>();
						String fd = new String(Globals.campaignfolderNAME);
						String[] files = FileHandling.listDir(fd);
						for ( int i = 0; i < files.length; i++ ) {
							Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(fd+files[i]+"/"+Globals.campaignFileName));
							campaignsList.add(Globals.allCampaigns.get(i).getCampaignName());
						}
						// we need to set up the active and selected campaign indexes again
						for (int r=0;r<Globals.allCampaigns.size();r++) {
							if (Globals.allCampaigns.get(r).getCampaignName().equals(Globals.activeCampaign.getCampaignName())) {
								Globals.currentCampaignIndex = r;
							}
						}
						Globals.selectedCampaignIndex = Globals.currentCampaignIndex;
						campaignsList.select(Globals.selectedCampaignIndex);
					}
				}); 
			}
			{
				FormData progressBarLData = new FormData();
				progressBarLData.left =  new FormAttachment(0, 1000, 4);
				progressBarLData.top =  new FormAttachment(0, 1000, 262);
				progressBarLData.width = 236;
				progressBarLData.height = 17;
				progressBar = new ProgressBar(this, SWT.NONE);
				progressBar.setLayoutData(progressBarLData);
			}
			{
				progressLabel = new Label(this, SWT.NONE);
				FormData progressLabelLData = new FormData();
				progressLabelLData.left =  new FormAttachment(0, 1000, 4);
				progressLabelLData.top =  new FormAttachment(0, 1000, 285);
				progressLabelLData.width = 456;
				progressLabelLData.height = 15;
				progressLabel.setLayoutData(progressLabelLData);
				progressLabel.setBackground(SWTResourceManager.getColor(255,238,219));
				progressLabel.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				deleteButton = new Button(this, SWT.PUSH | SWT.CENTER);
				deleteButton.setText("Delete Campaign");
				FormData deleteButtonLData = new FormData();
				deleteButtonLData.left =  new FormAttachment(0, 1000, 496);
				deleteButtonLData.top =  new FormAttachment(0, 1000, 238);
				deleteButtonLData.width = 175;
				deleteButtonLData.height = 25;
				deleteButton.setLayoutData(deleteButtonLData);
				deleteButton.setToolTipText("Permenantly delete a Campaign");
				deleteButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						DLG_Delete deleteWindow = new DLG_Delete(getShell(), SWT.Activate);
						deleteWindow.open();
						// repopulate the campaigns list
						Globals.allCampaigns.clear();
						String[] rList = FileHandling.listDir(Globals.campaignfolderNAME);
						for (int q=0;q<rList.length;q++) {
							Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(Globals.campaignfolderNAME+rList[q]+"/"+Globals.campaignFileName));
						}
						campaignsList.removeAll();
						for (int r=0;r<rList.length;r++) {
							campaignsList.add(Globals.allCampaigns.get(r).getCampaignName());
							if(Globals.allCampaigns.get(r).getCampaignName().equals(Globals.activeCampaign.getCampaignName())) {
								Globals.currentCampaignIndex = r;
								Globals.selectedCampaignIndex = r;
							}
						}
					}
				});
			}
			{
				renameButton = new Button(this, SWT.PUSH | SWT.CENTER);
				renameButton.setText("Rename Campaign");
				FormData renameButtonLData = new FormData();
				renameButtonLData.left =  new FormAttachment(0, 1000, 496);
				renameButtonLData.top =  new FormAttachment(0, 1000, 207);
				renameButtonLData.width = 175;
				renameButtonLData.height = 25;
				renameButton.setLayoutData(renameButtonLData);
				renameButton.setToolTipText("Change the name of campaigns.");
				renameButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						//First task, get the name of the campaign that's been selected
						String nameToChange = Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName();        						
						if (Globals.selectedCampaignIndex == Globals.currentCampaignIndex) {
							//we need to edit the campaign folder, and the xml files there and in the 
							// savegame folder
							// get the new name next
							Globals.newCampaignName = null;
							
							DLG_NewCampaignName newNameThing = new DLG_NewCampaignName(getShell(), SWT.Activate);
							newNameThing.open();
							if (Globals.newCampaignName != null) {
								if (!FileHandling.doesFolderExist(Globals.campaignfolderNAME+Globals.newCampaignName)) {
									FileHandling.createFolder(Globals.campaignfolderNAME+Globals.newCampaignName);
									// edit the stored campaign file to change the name
									Globals.activeCampaign.setCampaignName(Globals.newCampaignName);
									// write out a new campaign info file to the egosoft save folder
									CampaignFileHandling.writeRevisedCampaignFile(Globals.saveFolder+"/"+Globals.campaignFileName, Globals.activeCampaign);
									
									// copy all the campaign files into the new folder
									File[] dList = null;
									dList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
									progressBar.setMinimum(0);
									progressBar.setMaximum(dList.length);
									for (int i=0;i<dList.length;i++) {
										progressLabel.setText("Perfoming step one of rename operation");
										try {
											FileHandling.copySingleFile(dList[i].toString(),Globals.campaignfolderNAME+Globals.newCampaignName+"/"+dList[i].getName().toString());
										} catch (Exception e1) {
											Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
											Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+dList[i].toString());
											Globals.mBox.open();
											System.exit(0);
										}
										progressBar.setSelection(i);
									}
									// the new folder has been created, so we can delete the old one
									try {
										FileUtils.cleanDirectory(new File (Globals.campaignfolderNAME+nameToChange));
										FileUtils.deleteDirectory(new File(Globals.campaignfolderNAME+nameToChange));												
									} catch (IOException e1) {
										Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
										Globals.mBox.setMessage("Fatal Error: Savegame rename operation failed. The previous campaign folder is still present");
										Globals.mBox.open();
										System.exit(0);									}
									cActiveName.setText(Globals.activeCampaign.getCampaignName());

									progressLabel.setText("Rename operation completed.");
										progressBar.setSelection(0);
										String[] files = FileHandling.listDir(Globals.campaignfolderNAME);
										Globals.allCampaigns.clear();
										campaignsList.removeAll();
										for ( int d = 0; d < files.length; d++ ) {
											Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(Globals.campaignfolderNAME+files[d]+"/"+Globals.campaignFileName));
											campaignsList.add(Globals.allCampaigns.get(d).getCampaignName());
										}
										Globals.selectedCampaignIndex = 0;            					
										if (Globals.selectedCampaignIndex == Globals.currentCampaignIndex) {
											cTitle.setText(Globals.activeCampaign.getCampaignName());
											commentText.setText(Globals.activeCampaign.getLogFile());

										} else {
											cTitle.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName());
											commentText.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getLogFile());
										}

								} else {
									Globals.mBox = new MessageBox(getShell(), SWT.ICON_INFORMATION |SWT.OK);
									Globals.mBox.setMessage("That Campaign name is already in use, aborting operation.");
									Globals.mBox.open();
								}

							} else {
								Globals.mBox = new MessageBox(getShell(), SWT.ICON_INFORMATION |SWT.OK);
								Globals.mBox.setMessage("No new name entered, aborting operation.");
								Globals.mBox.open();
							}
						} else {
							// we only need to edit the files in the campaigns folder.
							// get the new name next
							Globals.newCampaignName = null;
							DLG_NewCampaignName newNameThing = new DLG_NewCampaignName(getShell(), SWT.Activate);
							newNameThing.open();
							if (Globals.newCampaignName != null) {
									if (!FileHandling.doesFolderExist(Globals.campaignfolderNAME+Globals.newCampaignName)) {
										FileHandling.createFolder(Globals.campaignfolderNAME+Globals.newCampaignName);
										// we need a temp folder now, so make it.
										FileHandling.createFolder(Globals.tempFolder);
										// edit the stored campaign file to change the name
										Globals.renameCampaign = CampaignFileHandling.loadCampaignFile(Globals.campaignfolderNAME+Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName()+"/"+Globals.campaignFileName);
										Globals.renameCampaign.setCampaignName(Globals.newCampaignName);

										// start by copying all the files into the egosoft/temp folder first.
										File[] dList = null;
										dList = FileHandling.listDirectoryContent(Globals.campaignfolderNAME+Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName(),Globals.campaignFileExtensions,false);
										progressBar.setMinimum(0);
										progressBar.setMaximum(dList.length);
										boolean check = true;
										boolean error = false;
										for (int i=0;i<dList.length;i++) {
											progressLabel.setText("Perfoming step one of rename operation");
											try {
												check = FileHandling.copySingleFile(dList[i].toString(),Globals.tempFolder+"/"+dList[i].getName().toString());
											    if (!check) {
											    	error = true;
											    }
											} catch (Exception e1) {
												Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
												Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+dList[i].toString());
												Globals.mBox.open();
												System.exit(0);
											}
											progressBar.setSelection(i);
										}
										// now copy the campaign into the new folder, provided there wasn't an error
										File[] dList2 = null;
										if (!error) {
											// revise the campaign info file
											CampaignFileHandling.writeRevisedCampaignFile(Globals.tempFolder+"/"+Globals.campaignFileName, Globals.renameCampaign);
											// collect up the filenames
											dList2 = FileHandling.listDirectoryContent(Globals.tempFolder,Globals.campaignFileExtensions,false);
											progressBar.setMinimum(0);
											progressBar.setMaximum(dList.length);
											boolean check2 = true;
											boolean error2 = false;
											for (int i=0;i<dList.length;i++) {
												progressLabel.setText("Perfoming step two of rename operation");
												try {
													check = FileHandling.copySingleFile(dList2[i].toString(),Globals.campaignfolderNAME+Globals.newCampaignName+"/"+dList2[i].getName().toString());
												    if (!check2) {
												    	error2 = true;
												    
												    }
												} catch (Exception e1) {
													Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
													Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+dList[i].toString());
													Globals.mBox.open();
													System.exit(0);
												}
												progressBar.setSelection(i);
											}
											if (!error2) {
												// now clear up the temporary campaign folder we just used and remove the old campaign folder
												try {
													FileUtils.cleanDirectory(new File (Globals.tempFolder));
													FileUtils.deleteDirectory(new File(Globals.tempFolder));
											
												} catch (IOException e1) {
													Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
													Globals.mBox.setMessage("Fatal Error: Savegame rename operation failed. The original campaign is still intact");
													Globals.mBox.open();
													System.exit(0);
												}
												try {
													FileUtils.cleanDirectory(new File (Globals.campaignfolderNAME+Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName()));
													FileUtils.deleteDirectory(new File(Globals.campaignfolderNAME+Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName()));	

												} catch (IOException e1) {
													Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR |SWT.OK);
													Globals.mBox.setMessage("Fatal Error: Savegame rename operation failed. The previous campaign folder is still present");
													Globals.mBox.open();
													System.exit(0);
												}
												progressLabel.setText("Rename operation completed.");
												progressBar.setSelection(0);
												String[] files = FileHandling.listDir(Globals.campaignfolderNAME);
												Globals.allCampaigns.clear();
												campaignsList.removeAll();
												for ( int d = 0; d < files.length; d++ ) {
													Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(Globals.campaignfolderNAME+files[d]+"/"+Globals.campaignFileName));
													campaignsList.add(Globals.allCampaigns.get(d).getCampaignName());
												}
												Globals.selectedCampaignIndex = 0;            					
												if (Globals.selectedCampaignIndex == Globals.currentCampaignIndex) {
													cTitle.setText(Globals.activeCampaign.getCampaignName());
													commentText.setText(Globals.activeCampaign.getLogFile());

												} else {
													cTitle.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName());
													commentText.setText(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getLogFile());
												}
											}
                                            
											
										}
										
										
										
					
							
									
											


									} else {
										Globals.mBox = new MessageBox(getShell(), SWT.ICON_INFORMATION |SWT.OK);
										Globals.mBox.setMessage("That Campaign name is already in use, aborting operation.");
										Globals.mBox.open();
									}

								} else {
									Globals.mBox = new MessageBox(getShell(), SWT.ICON_INFORMATION |SWT.OK);
									Globals.mBox.setMessage("No new name entered, aborting operation.");
									Globals.mBox.open();
								}
							} 					
						}
						
		
				});
			}
			{
				menu1 = new Menu(getShell(), SWT.BAR);
				getShell().setMenuBar(menu1);
				{
					fileMenuItem = new MenuItem(menu1, SWT.CASCADE);
					fileMenuItem.setText("File");
					{
						fileMenu = new Menu(fileMenuItem);
						{
							exitMenuItem = new MenuItem(fileMenu, SWT.CASCADE);
							exitMenuItem.setText("Exit");
							exitMenuItem.addSelectionListener(new SelectionAdapter() { 
								public void widgetSelected(SelectionEvent e) { 
									System.exit(0);
								} 
							}); 
						}
						fileMenuItem.setMenu(fileMenu);
					}
				}
				{
					webLinksItem = new MenuItem(menu1, SWT.CASCADE);
					webLinksItem.setText("Web Links");
					{
						menu2 = new Menu(webLinksItem);
						webLinksItem.setMenu(menu2);
						{
							forumPostItem = new MenuItem(menu2, SWT.PUSH);
							forumPostItem.setText("Visit the Savegame Managers forum post (includes usage guide)");
							forumPostItem.addSelectionListener(new SelectionAdapter() {
								public void widgetSelected(SelectionEvent evt) {
									forumPostItemWidgetSelected(evt);
								}
							});
						}
						{
							FAQItem = new MenuItem(menu2, SWT.PUSH);
							FAQItem.setText("Read the Savegame Managers FAQ");
							FAQItem.addSelectionListener(new SelectionAdapter() {
								public void widgetSelected(SelectionEvent evt) {
									FAQItemWidgetSelected(evt);
								}
							});
						}
						{
							mainXuForum = new MenuItem(menu2, SWT.PUSH);
							mainXuForum.setText("Visit the main X-Universe Forum");
							mainXuForum.addSelectionListener(new SelectionAdapter() {
								public void widgetSelected(SelectionEvent evt) {
									mainXuForumWidgetSelected(evt);
								}
							});
						}
					}
				}
				{
					aboutMenuItem = new MenuItem(menu1, SWT.CASCADE);
					aboutMenuItem.setText("About");
					aboutMenuItem.addSelectionListener(new SelectionAdapter() { 
						public void widgetSelected(SelectionEvent e) { 
							DLG_AboutBox about = new DLG_AboutBox(getShell(),SWT.APPLICATION_MODAL);
							about.open();
						} 
					}); 
					aboutMenuItem.setMenu(aboutMenu);
				}
			}
			this.layout();
			pack();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		Display display = Display.getDefault();
		Shell shell = new Shell(display);

		DLG_MainGiu mainWindow = new DLG_MainGiu(shell, SWT.NULL);
		Point size = mainWindow.getSize();
		shell.setLayout(new FillLayout());
		shell.layout();

		if(size.x == 0 && size.y == 0) {
			mainWindow.pack();
			shell.pack();
		} else {
			Rectangle shellBounds = shell.computeTrim(0, 0, size.x, size.y);
			shell.setSize(shellBounds.width, shellBounds.height);
		}
		// set up the title bar
		shell.setText(Globals.mainWIndowTitle);
		// Get the resolution
		Rectangle shellDisplayBounds = shell.getDisplay().getBounds();
		
		int shellMinWidth = 0;
		// calculate the shell's Left and Top
		int shellLeft = (shellDisplayBounds.width - shellMinWidth) / 2;
		int shellMinHeight = 0;
		int shellTop = (shellDisplayBounds.height - shellMinHeight) / 2;
		
		// Set shell bounds,
		shell.setBounds(shellLeft-410, shellTop-230, shellMinWidth+700, shellMinHeight+370);
		// load the icon from the jar
		InputStream stream = DLG_MainGiu.class.getResourceAsStream( "politeSpider.ico" );
		Image imgicon = new Image(shell.getDisplay(), stream);
		shell.setImage(imgicon);
		// show the application window
		shell.open();
		// grab the working directory and store it
		Globals.appHome = System.getProperty("user.dir")+"/";
		//set up the vault folder location string
		Globals.vaultfolderNAME = Globals.appHome+"Vault/";
		//set up the campaign folder location string we need for file copy operations
		Globals.campaignfolderNAME = Globals.appHome+"Campaigns/";
		//silently create the campaigns folder if it doesn't already exist
		if(!FileHandling.doesFolderExist(Globals.campaignfolderNAME)) {
			FileHandling.createFolder(Globals.campaignfolderNAME);
		}
		// silently create the cfg.xml file if it doesn't exist
		if(!FileHandling.doesFileExist("cfg.xml")) {
			CfgFileMaker.newCfgFile("cfg.xml");
		}
		// get the egosoft folder from my documents (we need this when renaming campaigns)
		// no error checking here, since this is something we set ourselves, and anyway,
		// other things will fail later if it doesn't exist
		SetVars.setEgosoftFolderLocation();
		SetVars.setTempFolderLocation();
		// Get the X3 savegame folder. It could be a non standard location
		// that the user has specified, so we check for both
		try {
			ConfigLoader.load("cfg.xml");
		} catch (Exception e2) {
			Globals.mBox = new MessageBox(shell, SWT.ICON_ERROR
			| SWT.OK);
			Globals.mBox.setMessage("Unable to load 'cfg.xml' file from the application directory\n");
			Globals.mBox.setText("Exiting Application");
			Globals.mBox.open(); 
			System.exit(0);
		}
		try {

          
			if (Globals.specifiedSaveFolder.equals(Globals.standard)) { // the user hasn't specified a new savegame folder
				// check to see whether there is a current X3 savegame folder
				SetVars.setSaveGameLocation();
				if(!FileHandling.doesFolderExist(Globals.saveFolder)) {{
						Globals.mBox = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
						Globals.mBox.setMessage(Globals.saveFolderNotFound);
						Globals.mBox.setText("Exiting Application");
						Globals.mBox.open(); 
						System.exit(0);
					}
				} 
			} else {// the user appears to have set their own savegame location, check that it exists
				if(!FileHandling.doesFolderExist(Globals.specifiedSaveFolder)) {
					Globals.mBox = new MessageBox(shell, SWT.ICON_ERROR
					| SWT.OK);
					Globals.mBox.setMessage("User specified savegame folder not found:\n"+Globals.specifiedSaveFolder);
					Globals.mBox.setText("Exiting Application");
					Globals.mBox.open(); 
					System.exit(0);
				} 
				// the user specified savegame location exists
				Globals.saveFolder = Globals.specifiedSaveFolder;
				// quick check, has the user correctly ended the location with a '/'?
				if ((!(Globals.saveFolder.charAt(Globals.saveFolder.length()-1) =='/'))&&(!(Globals.saveFolder.charAt(Globals.saveFolder.length()-1) =='\\'))) {
					Globals.saveFolder = Globals.saveFolder.concat("\\");
				}
			}
		} catch (Exception e2) {
			Globals.mBox = new MessageBox(shell, SWT.ICON_ERROR
			| SWT.OK);
			Globals.mBox.setMessage("Problem encountered when setting the location of the X3TC savegame folder.\n");
			Globals.mBox.setText("Exiting Application");
			Globals.mBox.open(); 
			System.exit(0);
		}
		// allocate the campaigns list
		Globals.allCampaigns = new ArrayList<CampaignInfo>();
		Globals.activeCampaign = new CampaignInfo();
		Globals.selectedCampaign = new CampaignInfo();
		
		String[] campaignsFolderContent = FileHandling.listDir(Globals.campaignfolderNAME);
		for (int t=0;t<campaignsFolderContent.length;t++) {
			try {
				Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(Globals.campaignfolderNAME+campaignsFolderContent[t]+"/"+Globals.campaignFileName));
			} catch (Exception e) {
				Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
				Globals.mBox.setMessage("Fatal Error: The campaign data file 'campaign file.xml' is missing for the following stored Campaign: "+Globals.campaignfolderNAME+campaignsFolderContent[t]);
				Globals.mBox.open();
				System.exit(0);
			}
		}
		// ok, next check, are there any savegames in the savegame folder?
		File[] saveFolderContent = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
		if(saveFolderContent.length ==0) {
			// Ok, no savegames, but is there already a campaigns folder with campaigns in?
			String[] filesx = FileHandling.listDir(Globals.campaignfolderNAME);
			if (filesx.length!=0) {
				Globals.mBox = new MessageBox(shell, SWT.ICON_INFORMATION |SWT.YES | SWT.NO);
				Globals.mBox.setMessage("Your savegame folder is empty, but a campaign folder with available campaigns has been found. Do you want to select a campaign to activate?");
				int ans = Globals.mBox.open();
				if (ans == SWT.NO) {
					Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_INFORMATION |SWT.OK);
					Globals.mBox.setMessage("Please either start a new X3 game and make at least one save, or put some savegame files in your savegame folder.\nThis program will now exit.");
					Globals.mBox.open();
					System.exit(0);
				}
				if (ans == SWT.YES) {
					Globals.pickedCampaignIndex =-1;
					// set up the text for the dialog we are about to display
					Globals.campaignSelectBoxText = Globals.selectCampaignText;
					DLG_CampaignSelectBox cSBox = new DLG_CampaignSelectBox(mainWindow.getShell(),SWT.APPLICATION_MODAL);
					cSBox.open();
					if (Globals.pickedCampaignIndex ==-1){
						Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_INFORMATION |SWT.OK);
						Globals.mBox.setMessage("No Campaign selected, defaulting to the first on the list.");
						Globals.mBox.open();
						Globals.pickedCampaignIndex = 0;
					}
					File[] chosenCampaignFileList = FileHandling.listDirectoryContent(Globals.campaignfolderNAME+Globals.allCampaigns.get(Globals.pickedCampaignIndex).getCampaignName(),Globals.campaignFileExtensions,false);
					if((chosenCampaignFileList.length == 0)||(chosenCampaignFileList.length == 1)) {
						Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
						Globals.mBox.setMessage("The selected campaign appears to have no savegame files in its storage folder.\n Please check this.\n As this is an issue that may cause problems for the Savegame Manager, it will now exit");
						Globals.mBox.open();
						System.exit(0);
					}
					// clear the savegame folder (just in case)
					FileHandling.emptyDirectory(Globals.saveFolder);
					// and move the newly activated campaign into the save folder.
					mainWindow.progressBar.setMinimum(0);
					mainWindow.progressBar.setMaximum(chosenCampaignFileList.length);
					for (int i=0;i<chosenCampaignFileList.length;i++) {
						mainWindow.progressLabel.setText("transfering the activated campaign into the X3 savegame folder: "+chosenCampaignFileList[i].getName());
						try {
							FileHandling.copySingleFile(chosenCampaignFileList[i].toString(),Globals.saveFolder+"/"+chosenCampaignFileList[i].getName());
						} catch (Exception e1) {
							Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
							Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+chosenCampaignFileList[i].toString());
							Globals.mBox.open();
							System.exit(0);
						}
						mainWindow.progressBar.setSelection(i);
					}
					mainWindow.progressLabel.setText("Selected campaign activated");
					mainWindow.progressBar.setSelection(0);
					mainWindow.exitButton.setEnabled(true);
					mainWindow.editComment.setEnabled(true);
					Globals.activeCampaign.setCampaignName(Globals.allCampaigns.get(Globals.pickedCampaignIndex).getCampaignName());
					Globals.activeCampaign.setLogFile(Globals.allCampaigns.get(Globals.pickedCampaignIndex).getLogFile());
					Globals.currentCampaignIndex = Globals.pickedCampaignIndex;
					Globals.selectedCampaign.setCampaignName(Globals.allCampaigns.get(Globals.pickedCampaignIndex).getCampaignName());
					Globals.selectedCampaign.setLogFile(Globals.allCampaigns.get(Globals.pickedCampaignIndex).getLogFile());
					Globals.selectedCampaignIndex = Globals.pickedCampaignIndex;
					mainWindow.cActiveName.setText(Globals.activeCampaign.getCampaignName());
					mainWindow.commentText.setText(Globals.activeCampaign.getLogFile());
					mainWindow.cTitle.setText(Globals.activeCampaign.getCampaignName());
					
					
				}	
			}
		} else { //There are some savegames
			// Are they already a managed campaign?
			boolean noTheyArent = true;
			for (int d = 0;d<saveFolderContent.length;d++) {
				if(saveFolderContent[d].getName().equals(Globals.campaignFileName)) {
					noTheyArent = false;
				}
			}
			if(noTheyArent) { // this isn't a managed campaign, so lets make it one. 
				Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_QUESTION  |SWT.YES | SWT.NO);
				Globals.mBox.setMessage("Would you like Savegame Manager to manage the current savegame set?");
				int rx = Globals.mBox.open();
				if (rx ==  SWT.NO) {
					System.exit(0);
				}
				// null the fields we will need to be checking later to ensure data was entered
				Globals.campaignNameStorage = null;
				Globals.commentStorage = null;
				DLG_CampaignSetup setupBox = new DLG_CampaignSetup(shell,1);
				setupBox.open();
				//check to see that we have a campaign name specified, if not, exit (null or empty)
				if (Globals.campaignNameStorage ==null) {
					Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
					Globals.mBox.setMessage("Campaign name not properly set - aborting operation");
					Globals.mBox.open();
					System.exit(0);
				}
				if(Globals.campaignNameStorage.length()==0) {
					Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
					Globals.mBox.setMessage("Campaign name empty after invalid filename character stripping - aborting operation");
					Globals.mBox.open();
					System.exit(0);
				}
				//compare the chosen name with the names of pre-existing campaigns (if any)
				String[] filesx2 = FileHandling.listDir(Globals.campaignfolderNAME);
				if (filesx2.length!=0) {
					ArrayList<CampaignInfo> tmpCampaigns = new ArrayList<CampaignInfo>();

					for ( int i = 0; i <filesx2.length; i++ ) {
						try {
							tmpCampaigns.add(CampaignFileHandling.loadCampaignFile(Globals.campaignfolderNAME+filesx2[i]+"/"+Globals.campaignFileName));
						} catch (Exception e) {
							Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
							Globals.mBox.setMessage("Fatal Error: The campaign data file 'campaign file.xml' is missing for the following stored Campaign: "+Globals.campaignfolderNAME+campaignsFolderContent[i]);
							Globals.mBox.open();
							System.exit(0);
						}
					}
					for (int g=0;g<filesx2.length;g++) {
						if (tmpCampaigns.get(g).getCampaignName().equals(Globals.campaignNameStorage)) {
							Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
							Globals.mBox.setMessage("Specified campaign name is already in use - aborting operation");
							Globals.mBox.open();
							System.exit(0);
						}
					}
					tmpCampaigns.clear();
				}
				// ok we got this far, the information to create a campaign file exists, and isn't already in use
				Globals.activeCampaign = new CampaignInfo();
				Globals.activeCampaign.setCampaignName(Globals.campaignNameStorage);
				if (Globals.commentStorage == null) {
					Globals.activeCampaign.setLogFile("");
				} else {
					Globals.activeCampaign.setLogFile(Globals.commentStorage);
				}
				Globals.activeCampaign.setLogEntryNumber(Globals.startingLogNumber);
				Globals.activeCampaign.setLogEntryTemplate(Globals.defaultLogEntry);
				Globals.activeCampaign.setX3SGMversion(Globals.campaignFileversion);
				// write the campaign file to the savegame folder
				try {
					CampaignFileHandling.newCampaignFile(Globals.saveFolder+Globals.campaignFileName,Globals.activeCampaign.getCampaignName(), Globals.activeCampaign.getLogFile(),Globals.activeCampaign.getLogEntryNumber(),Globals.activeCampaign.getLogEntryTemplate());
				} catch (Exception e1) {
					Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
					Globals.mBox.setMessage("Unable to create campaign file - The Savegame Manager cannot continue.");
					Globals.mBox.open();
					System.exit(0);
				}
				// The campaign file has been created, so we need to load it in now.
				Globals.activeCampaign = CampaignFileHandling.loadCampaignFile(Globals.saveFolder+Globals.campaignFileName);
				// and make the folder that will house this campaign and its restore points
				FileHandling.createFolder(Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName());
				// and copy the files from the save folder into this newly created store.
				mainWindow.exitButton.setEnabled(false);
				mainWindow.editComment.setEnabled(false);
				File[] tmpFileList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
				mainWindow.progressBar.setMinimum(0);
				mainWindow.progressBar.setMaximum(tmpFileList.length);
				for (int i=0;i<tmpFileList.length;i++) {
					mainWindow.progressLabel.setText("Copying the newly created campaign into the campaign folder: "+tmpFileList[i].getName());
					try {
						FileHandling.copySingleFile(tmpFileList[i].toString(),Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName()+"/"+tmpFileList[i].getName().toString());
					} catch (Exception e) {
						Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
						Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
						Globals.mBox.open();
						System.exit(0);
					}
					mainWindow.progressBar.setSelection(i);
				}
				mainWindow.progressLabel.setText("");
				mainWindow.progressBar.setSelection(0);
				mainWindow.progressLabel.setText("Campaign succesfully created.");
				mainWindow.exitButton.setEnabled(true);
				mainWindow.editComment.setEnabled(true);
			} else { // it is already a managed campaign
				// Load in its campaign info file
				Globals.activeCampaign = CampaignFileHandling.loadCampaignFile(Globals.saveFolder+Globals.campaignFileName);
				
				// check to see whether there is a copy of it in the campaigns folder
				boolean checking = false;
				String[] filesx2 = FileHandling.listDir(Globals.campaignfolderNAME);
				ArrayList<CampaignInfo> tmpCampaigns = new ArrayList<CampaignInfo>();
				if (filesx2.length!=0) {
					for ( int i = 0; i <filesx2.length; i++ ) {
						try {
							tmpCampaigns.add(CampaignFileHandling.loadCampaignFile(Globals.campaignfolderNAME+filesx2[i]+"/"+Globals.campaignFileName));
						} catch (Exception e) {
							Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
							Globals.mBox.setMessage("Fatal Error: The campaign data file 'campaign file.xml' is missing for the following stored Campaign: "+Globals.campaignfolderNAME+filesx2[i]);					
							Globals.mBox.open();
							System.exit(0);
						}
					}
					for (int g=0;g<filesx2.length;g++) {
						if (tmpCampaigns.get(g).getCampaignName().equals(Globals.activeCampaign.getCampaignName())) {
							checking = true;
						}
					}
				} 
				tmpCampaigns.clear();
				if (checking ==false) {// no copy found in the campains storage folder, make one
					mainWindow.exitButton.setEnabled(false);
					mainWindow.editComment.setEnabled(false);
					File[] tmpFileList = FileHandling.listDirectoryContent(Globals.saveFolder,Globals.campaignFileExtensions,false);
					if((tmpFileList.length == 0)||(tmpFileList.length == 1)) {
						Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
						Globals.mBox.setMessage("The currently active campaign appears to have no savegame files in the X3TC savegame folder.\n Please check this.  The Savegame Manager will now exit.");
						Globals.mBox.open();
						System.exit(0);
					}
					mainWindow.progressBar.setMinimum(0);
					mainWindow.progressBar.setMaximum(tmpFileList.length);
					for (int i=0;i<tmpFileList.length;i++) {
						mainWindow.progressLabel.setText("Storing the active campaign in your Campaigns folder: "+tmpFileList[i].getName());
						try {
							FileHandling.copySingleFile(tmpFileList[i].toString(),Globals.campaignfolderNAME+Globals.activeCampaign.getCampaignName()+"/"+tmpFileList[i].getName().toString());
						} catch (Exception e1) {
							Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
							Globals.mBox.setMessage("Fatal Error: Savegame copy operation failed while copying: "+tmpFileList[i].toString());
							Globals.mBox.open();
							System.exit(0);
						}
						mainWindow.progressBar.setSelection(i);
					}
					mainWindow.progressLabel.setText("Active campaign now stored in campaigns folder");
					mainWindow.progressBar.setSelection(0);
					mainWindow.exitButton.setEnabled(true);
					mainWindow.editComment.setEnabled(true);
				}
				//make it active and selected in the manager
			}
		}

		// Ok, the essential business of setting up an active campaign is done, so its time to
		// finally populate the campaigns list
		Globals.allCampaigns.clear();
		String[] dList = FileHandling.listDir(Globals.campaignfolderNAME);
		for (int q=0;q<dList.length;q++) {
			Globals.allCampaigns.add(CampaignFileHandling.loadCampaignFile(Globals.campaignfolderNAME+dList[q]+"/"+Globals.campaignFileName));
		}
		mainWindow.campaignsList.removeAll();
		for (int r=0;r<dList.length;r++) {
			mainWindow.campaignsList.add(Globals.allCampaigns.get(r).getCampaignName());
			if(Globals.allCampaigns.get(r).getCampaignName().equals(Globals.activeCampaign.getCampaignName())) {
				Globals.currentCampaignIndex = r;
				Globals.selectedCampaignIndex = r;
			}
		}		
		// check to see whether the savegame folder and the campaigns folder were both empty
		if((Globals.allCampaigns.size()==0)&&(saveFolderContent.length==0)) {
			Globals.mBox = new MessageBox(mainWindow.getShell(), SWT.ICON_ERROR |SWT.OK);
			Globals.mBox.setMessage("No Savegames or managed campaigns found.\n You need one or the other to start this application.");
			Globals.mBox.open();
			System.exit(0);
		} else {
			Globals.selectedCampaign.setCampaignName(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getCampaignName());
			Globals.selectedCampaign.setLogFile(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getLogFile());
			Globals.selectedCampaign.setLogEntryTemplate(Globals.allCampaigns.get(Globals.selectedCampaignIndex).getLogEntryTemplate());
			mainWindow.cActiveName.setText(Globals.activeCampaign.getCampaignName());
			mainWindow.commentText.setText(Globals.activeCampaign.getLogFile());
			mainWindow.cTitle.setText(Globals.activeCampaign.getCampaignName());
			// write it back, just in case a it has been converted to a new file format.
			CampaignFileHandling.newCampaignFile(Globals.saveFolder+Globals.campaignFileName,Globals.activeCampaign.getCampaignName(), Globals.activeCampaign.getLogFile(),Globals.activeCampaign.getLogEntryNumber(),Globals.activeCampaign.getLogEntryTemplate());
			mainWindow.activateCampaignButton.setEnabled(false);
		}
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
			display.sleep();
		}
	}

	private void forumPostItemWidgetSelected(SelectionEvent evt) {
		try {
			Desktop.getDesktop().browse(new URI("http://forum.egosoft.com/viewtopic.php?t=249129&start=0&postdays=0&postorder=asc&highlight="));
		} catch (Exception e) {
			Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR
			| SWT.OK);
			Globals.mBox.setMessage("Cannot open default browser to display forum post");
			Globals.mBox.open(); 
		}
	}
	
	private void FAQItemWidgetSelected(SelectionEvent evt) {
		try {
			Desktop.getDesktop().browse(new URI("http://forum.egosoft.com/viewtopic.php?p=2917207&highlight=#2917207"));
		} catch (Exception e) {
			Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR
			| SWT.OK);
			Globals.mBox.setMessage("Cannot open default browser to display forum post");
			Globals.mBox.open(); 
		}
	}
	
	private void mainXuForumWidgetSelected(SelectionEvent evt) {
		try {
			Desktop.getDesktop().browse(new URI("http://forum.egosoft.com/viewforum.php?f=2"));
		} catch (Exception e) {
			Globals.mBox = new MessageBox(getShell(), SWT.ICON_ERROR
			| SWT.OK);
			Globals.mBox.setMessage("Cannot open default browser to display forum post");
			Globals.mBox.open(); 
		}
	}
}
