package com.politespider.X3.savegame.manager;

/**
* @author Carey
*  locate and set all the global vars we are going to need, 
*  such as the users home folder, their savegame location, and other bits
*  
*/
public class SetVars {
	public static String getUserHome() {
		return System.getenv("HOMEPATH");
	}
	public static String setSaveGameLocation() {
		String saveLocation = new String(""+javax.swing.filechooser.FileSystemView.getFileSystemView().getDefaultDirectory());
		saveLocation = saveLocation+Globals.gameSaveFolderLocation;
		Globals.saveFolder = saveLocation;
		return saveLocation;
	}
	public static String setEgosoftFolderLocation() {
		String egosoftFolderLocation = new String(""+javax.swing.filechooser.FileSystemView.getFileSystemView().getDefaultDirectory());
		egosoftFolderLocation = egosoftFolderLocation+"\\Egosoft\\";
		Globals.egosoftFolder = egosoftFolderLocation;
		return egosoftFolderLocation;
	}
	public static String setTempFolderLocation() {
		String tempFolderLocation = new String(""+javax.swing.filechooser.FileSystemView.getFileSystemView().getDefaultDirectory());
		tempFolderLocation = tempFolderLocation+"\\Egosoft\\temp\\";
		Globals.tempFolder = tempFolderLocation;
		return tempFolderLocation;
	}
}
