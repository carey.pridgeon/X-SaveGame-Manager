package com.politespider;
import java.awt.Desktop;
import java.net.URI;

import com.cloudgarden.resource.SWTResourceManager;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;



/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class DLG_AboutBox extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private StyledText styledText1;
	private Button closeButton;
	private StyledText styledText2;
	private Label label1;
	private Label label3;
	private Label label2;
	private Link link1;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_AboutBox inst = new DLG_AboutBox(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_AboutBox(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			
			
			dialogShell.setLayout(new FormLayout());
			dialogShell.layout();
			dialogShell.pack();			
			dialogShell.setSize(300, 275);
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));
			{
				label3 = new Label(dialogShell, SWT.NONE);
				label3.setText("All Rights Reserved");
				FormData label3LData = new FormData();
				label3LData.width = 100;
				label3LData.height = 15;
				label3LData.left =  new FormAttachment(0, 1000, 18);
				label3LData.top =  new FormAttachment(0, 1000, 100);
				label3.setLayoutData(label3LData);
				label3.setBackground(SWTResourceManager.getColor(255,238,219));
				label3.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				label2 = new Label(dialogShell, SWT.NONE);
				label2.setText("Copyright 2009-2010 Dr Carey Pridgeon ");
				FormData label2LData = new FormData();
				label2LData.width = 240;
				label2LData.height = 15;
				label2LData.left =  new FormAttachment(0, 1000, 18);
				label2LData.top =  new FormAttachment(0, 1000, 79);
				label2.setLayoutData(label2LData);
				label2.setBackground(SWTResourceManager.getColor(255,238,219));
				label2.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				label1 = new Label(dialogShell, SWT.NONE);
				label1.setText("A product of ");
				FormData label1LData = new FormData();
				label1LData.width = 70;
				label1LData.height = 15;
				label1LData.left =  new FormAttachment(0, 1000, 18);
				label1LData.top =  new FormAttachment(0, 1000, 58);
				label1.setLayoutData(label1LData);
				label1.setBackground(SWTResourceManager.getColor(255,238,219));
				label1.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				styledText2 = new StyledText(dialogShell, SWT.READ_ONLY | SWT.WRAP);
				styledText2.setText("This software and its author are in no way affiliated with Egosoft. \n\nThis software is provided free of charge and without warranty. ");
				FormData styledText2LData = new FormData();
				styledText2LData.width = 259;
				styledText2LData.height = 77;
				styledText2LData.left =  new FormAttachment(0, 1000, 18);
				styledText2LData.top =  new FormAttachment(0, 1000, 121);
				styledText2.setLayoutData(styledText2LData);
				styledText2.setBackground(SWTResourceManager.getColor(255,238,219));
				styledText2.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
				styledText2.setEnabled(false);
			}
			{
				closeButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData closeButtonLData = new FormData();
				closeButtonLData.width = 41;
				closeButtonLData.height = 25;
				closeButtonLData.left =  new FormAttachment(0, 1000, 118);
				closeButtonLData.top =  new FormAttachment(0, 1000, 204);
				closeButton.setLayoutData(closeButtonLData);
				closeButton.setText("Close");
				closeButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						dialogShell.dispose();
					}
				});

			}
			{
				link1 = new Link(dialogShell, SWT.NONE);
				link1.setText("<a href=\"http://politespider.com\">PoliteSpider Software</a>");
				FormData link1LData = new FormData();
				link1LData.width = 184;
				link1LData.height = 15;
				link1LData.left =  new FormAttachment(0, 1000, 88);
				link1LData.top =  new FormAttachment(0, 1000, 58);
				link1.setLayoutData(link1LData);
				link1.setBackground(SWTResourceManager.getColor(255,238,219));
				link1.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent evt) {
						link1WidgetSelected(evt);
					}
				});
			}
			{
				styledText1 = new StyledText(dialogShell, SWT.NONE);
				FormData styledText1LData = new FormData();
				styledText1LData.width = 253;
				styledText1LData.height = 46;
				styledText1LData.left =  new FormAttachment(0, 1000, 18);
				styledText1LData.top =  new FormAttachment(0, 1000, 6);
				styledText1.setLayoutData(styledText1LData);
				styledText1.setText(Globals.aboutVersionInfo);
				styledText1.setBackground(SWTResourceManager.getColor(255,238,219));
				styledText1.setWordWrap(true);
				styledText1.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
				styledText1.setEnabled(false);
			}
			dialogShell.setLocation(getParent().toDisplay(100,0));
			dialogShell.open();
			Display display = dialogShell.getDisplay();
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
				display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void link1WidgetSelected(SelectionEvent evt) {
		try {
			Desktop.getDesktop().browse(new URI("http://www.politespider.com"));
		} catch (Exception e) {
			Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_ERROR
			| SWT.OK);
			Globals.mBox.setMessage("Cannot open default browser to display link");
			Globals.mBox.open(); 
		}
	}
}
