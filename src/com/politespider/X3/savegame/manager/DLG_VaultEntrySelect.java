package com.politespider.X3.savegame.manager;
import com.cloudgarden.resource.SWTResourceManager;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;



public class DLG_VaultEntrySelect extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private Label VaultEntrySelectLabel;
	private Button doneButton;
	private List vaultEntrySelectList;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_VaultEntrySelect inst = new DLG_VaultEntrySelect(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_VaultEntrySelect(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			
			
			dialogShell.setLayout(new FormLayout());
			dialogShell.layout();
			dialogShell.pack();			
			dialogShell.setSize(273, 335);
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));
			{
				doneButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData doneButtonLData = new FormData();
				doneButtonLData.width = 40;
				doneButtonLData.height = 25;
				doneButtonLData.left =  new FormAttachment(0, 1000, 109);
				doneButtonLData.top =  new FormAttachment(0, 1000, 269);
				doneButton.setLayoutData(doneButtonLData);
				doneButton.setText("Done");
				doneButton.addSelectionListener(new SelectionAdapter() { 
					public void widgetSelected(SelectionEvent e) {
						// in this case we want an easy dropout if the user has got cold feet over the deletion
						// so failure to select a campaign is taken to be a desire not to delete one after all.
						Globals.vaultSelectedIndex = vaultEntrySelectList.getSelectionIndex();
						dialogShell.dispose();
					}
				});
			}
			{
				FormData vaultEntrySelectListLData = new FormData();
				vaultEntrySelectListLData.width = 213;
				vaultEntrySelectListLData.height = 230;
				vaultEntrySelectListLData.left =  new FormAttachment(0, 1000, 12);
				vaultEntrySelectListLData.top =  new FormAttachment(0, 1000, 33);
				vaultEntrySelectList = new List(dialogShell, SWT.V_SCROLL);
				vaultEntrySelectList.setLayoutData(vaultEntrySelectListLData);
				vaultEntrySelectList.setFont(SWTResourceManager.getFont("Segoe UI", 9, 3, false, false));
				vaultEntrySelectList.setToolTipText("This is a list of all available vault entries");
			}
			{
				VaultEntrySelectLabel = new Label(dialogShell, SWT.NONE);
				FormData VaultEntrySelectLabelLData = new FormData();
				VaultEntrySelectLabelLData.width = 191;
				VaultEntrySelectLabelLData.height = 15;
				VaultEntrySelectLabelLData.left =  new FormAttachment(0, 1000, 12);
				VaultEntrySelectLabelLData.top =  new FormAttachment(0, 1000, 12);
				VaultEntrySelectLabel.setLayoutData(VaultEntrySelectLabelLData);
				VaultEntrySelectLabel.setText("Select a vault entry");
				VaultEntrySelectLabel.setBackground(SWTResourceManager.getColor(255,238,219));
				VaultEntrySelectLabel.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			dialogShell.setLocation(getParent().toDisplay(0,0));
			dialogShell.open();
			for (int i=0;i<Globals.vaultContents.size();i++) {
				vaultEntrySelectList.add(Globals.vaultContents.get(i).getCampaignName());
			}
			Display display = dialogShell.getDisplay();
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
				display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
