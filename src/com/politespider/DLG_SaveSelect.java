package com.politespider;
import com.cloudgarden.resource.SWTResourceManager;

import java.io.File;

import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class DLG_SaveSelect extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private Table savesList;
	private Button selectAllSavesButton;
	private StyledText styledText1;
	private Button doneButton;
	private File[] saveFileList;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_SaveSelect inst = new DLG_SaveSelect(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_SaveSelect(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			
			
			dialogShell.setLayout(new FormLayout());
			{
				selectAllSavesButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData selectAllSavesButtonLData = new FormData();
				selectAllSavesButtonLData.width = 115;
				selectAllSavesButtonLData.height = 25;
				selectAllSavesButtonLData.left =  new FormAttachment(0, 1000, 122);
				selectAllSavesButtonLData.top =  new FormAttachment(0, 1000, 275);
				selectAllSavesButton.setLayoutData(selectAllSavesButtonLData);
				selectAllSavesButton.setText("Select All");
				selectAllSavesButton.setToolTipText("This will select every savegame in the above list");
				selectAllSavesButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						for (int d=0;d<savesList.getItemCount();d++) {
							savesList.getItem(d).setChecked(true);
						}      
					}
				});
			}
			{
				styledText1 = new StyledText(dialogShell, SWT.READ_ONLY | SWT.WRAP);
				FormData styledText1LData = new FormData();
				styledText1LData.width = 160;
				styledText1LData.height = 263;
				styledText1LData.left =  new FormAttachment(0, 1000, 243);
				styledText1LData.top =  new FormAttachment(0, 1000, 12);
				styledText1.setLayoutData(styledText1LData);
				styledText1.setText("Pick the SaveGames to archive from the list on the left. Check the boxes for all you wish to include. \n\nThe saves are listed numerically, and show the date they were last updated.\n\nIf you don't wish to add every savegame to the archived copy of the selected campaign, pick a few of the most recent savegames instead.");
				styledText1.setBackground(SWTResourceManager.getColor(255,238,219));
			}
			{
				FormData savesListLData = new FormData();
				savesListLData.width = 214;
				savesListLData.height = 246;
				savesListLData.left =  new FormAttachment(0, 1000, 6);
				savesListLData.top =  new FormAttachment(0, 1000, 12);
				savesList = new Table(dialogShell, SWT.CHECK);
				savesList.setLayoutData(savesListLData);
				savesList.setFont(SWTResourceManager.getFont("Segoe UI", 9, 3, false, false));
			}
			{
				doneButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				doneButton.setText("Done");
				FormData doneButtonLData = new FormData();
				doneButtonLData.width = 60;
				doneButtonLData.height = 25;
				doneButtonLData.left =  new FormAttachment(0, 1000, 344);
				doneButtonLData.top =  new FormAttachment(0, 1000, 275);
				doneButton.setLayoutData(doneButtonLData);
				doneButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) { 
						// collect up all the selected saves
						for (int j=0;j<savesList.getItemCount();j++) {
							if(savesList.getItem(j).getChecked()==true) {
								Globals.saveGameSet.add(saveFileList[j]);
							}
						}
						dialogShell.dispose();
					}
				});
				
			}
			dialogShell.layout();
			dialogShell.pack();			
			dialogShell.setSize(420, 338);
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));
			styledText1.setBackground(SWTResourceManager.getColor(255,238,219));
			styledText1.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			dialogShell.setLocation(getParent().toDisplay(0,0));
			dialogShell.open();
			// collect up all the savegames in the selected campaigns folder
			saveFileList = FileHandling.listDirectoryContent(Globals.campaignfolderNAME+Globals.allCampaigns.get(Globals.pickedCampaignIndex).getCampaignName()+"/",Globals.saveFileExtension,false);
			
			for (int i=0;i<saveFileList.length;i++) {
				TableItem item = new TableItem(savesList,SWT.CHECK);
				item.setText(saveFileList[i].getName().toString()+ " - "+DateMaker.convertToReadableDate(saveFileList[i].lastModified()));
				item.setChecked(false);
			}
			Display display = dialogShell.getDisplay();
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
				display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
