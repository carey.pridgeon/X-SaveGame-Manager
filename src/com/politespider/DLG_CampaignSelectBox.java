package com.politespider;
import com.cloudgarden.resource.SWTResourceManager;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;



public class DLG_CampaignSelectBox extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private List selectCampaignList;
	private Label exportSelectLabel;
	private Button doneButton;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_CampaignSelectBox inst = new DLG_CampaignSelectBox(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_CampaignSelectBox(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			
			
			dialogShell.setLayout(new FormLayout());
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));
			{
				doneButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData doneButtonLData = new FormData();
				doneButtonLData.width = 40;
				doneButtonLData.height = 25;
				doneButtonLData.left =  new FormAttachment(0, 1000, 94);
				doneButtonLData.top =  new FormAttachment(0, 1000, 246);
				doneButton.setLayoutData(doneButtonLData);
				doneButton.setText("Done");
				doneButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) { 
						dialogShell.dispose();
					}
				});
			}
			{
				exportSelectLabel = new Label(dialogShell, SWT.NONE);
				FormData exportSelectLabelLData = new FormData();
				exportSelectLabelLData.width = 207;
				exportSelectLabelLData.height = 15;
				exportSelectLabelLData.left =  new FormAttachment(0, 1000, 12);
				exportSelectLabelLData.top =  new FormAttachment(0, 1000, 12);
				exportSelectLabel.setLayoutData(exportSelectLabelLData);
				exportSelectLabel.setBackground(SWTResourceManager.getColor(255,238,219));
				exportSelectLabel.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
				// displayed text varies according to the operation
				exportSelectLabel.setText(Globals.campaignSelectBoxText);
			}
			{
				FormData selectCampaignListLData = new FormData();
				selectCampaignListLData.width = 189;
				selectCampaignListLData.height = 206;
				selectCampaignListLData.left =  new FormAttachment(0, 1000, 10);
				selectCampaignListLData.top =  new FormAttachment(0, 1000, 33);
				selectCampaignList = new List(dialogShell, SWT.V_SCROLL);
				selectCampaignList.setLayoutData(selectCampaignListLData);
				selectCampaignList.setFont(SWTResourceManager.getFont("Segoe UI", 9, 3, false, false));
				selectCampaignList.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) { 
						Globals.pickedCampaignIndex = selectCampaignList.getSelectionIndex();
					}
				});
			}
			dialogShell.layout();
			dialogShell.pack();			
			dialogShell.setSize(247, 314);
			dialogShell.setToolTipText("This is a list of all the campaigns you have available");
			dialogShell.setLocation(getParent().toDisplay(100,0));
			dialogShell.open();
			//populate the list
			for (int i=0;i<Globals.allCampaigns.size();i++) {
				selectCampaignList.add(Globals.allCampaigns.get(i).getCampaignName());
			}
			Display display = dialogShell.getDisplay();
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
				display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
