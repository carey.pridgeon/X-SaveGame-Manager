package com.politespider.X3.savegame.manager;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.XMLEvent;

public class VaultFileHandling {
	
	public static CampaignInfo loadVaultFile (String filename) {
		XMLEventReader eventReader = null;
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		InputStream in = null;
		try {
			in = new FileInputStream(filename);
		} catch (Exception e) {
			System.out.println("file not loaded");
		}
		try {
			eventReader = inputFactory.createXMLEventReader(in);
		} catch (Exception e) {	
			System.out.println("event reader error");
		}
		CampaignInfo result = new CampaignInfo();
		// Now its been loaded, its time to read the XML document
		while (eventReader.hasNext()) {
			XMLEvent event = null;
			try {
				event = eventReader.nextEvent();
			} catch (Exception e) {
				System.out.println("eventReader.nextEvent error");
			}
			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart() == ("VFversion")) {
					try {
						event = eventReader.nextEvent();
					} catch (Exception e) {
						System.out.println("problem in VFversion event");
					}
					result.setX3SGMversion(Double.valueOf(event.toString()));
					if (result.getX3SGMversion()==1.0) {
						result.setLogEntryNumber(1);
						result.setLogEntryTemplate(Globals.defaultLogEntry);
					}
				}
			}
			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart() == ("campaignName")) {
					try {
						event = eventReader.nextEvent();
					} catch (Exception e) {
						System.out.println("problem in campaignName event");
					}
					result.setCampaignName(event.toString());
				}
			}		
			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart() == ("description")) {
					try {
						event = eventReader.nextEvent();
					} catch (Exception e) {
						System.out.println("problem in description event");
					}
					result.setLogFile(event.toString());
				}
			}
		}
		return result;
	}

	public static void newVaultFile ( String destinationFile,String title, String comment) {
		BufferedWriter xmlOut = null;
		File fName = new File(destinationFile);
		try {
			xmlOut = new BufferedWriter(new FileWriter(fName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("<root xsi:noNamespaceSchemaLocation = \"X3SaveGameManager.xsd\" xmlns:xsi = \"http://www.w3.org/2001/XMLSchema-instance\">\n"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <VFversion>"+Globals.vaultFileversion+"</VFversion>\n"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <campaignName><![CDATA["+title+"]]></campaignName>\n"));
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("    <description><![CDATA["+comment+"]]></description>\n"));		
			xmlOut.write(FileHandling.convertToCrossPlatformNewline("</root>"));
			xmlOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
