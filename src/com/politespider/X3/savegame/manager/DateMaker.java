package com.politespider.X3.savegame.manager;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateMaker {
	
	public static String getDateStamp() {
		Date myDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd:HH-mm-ss");
		String myDateString = sdf.format(myDate);
		return myDateString;
	}
	
	public static String convertToReadableDate(long updatePoint) {
		Date myDate = new Date(updatePoint);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd: HH-mm");
		String myDateString = sdf.format(myDate);
		return myDateString;
	}
}
