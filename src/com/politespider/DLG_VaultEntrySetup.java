package com.politespider;
import com.cloudgarden.resource.SWTResourceManager;

import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;


public class DLG_VaultEntrySetup extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private Label vaultEntryNameLabel;
	private StyledText vaultEntryCommentInput;
	private Button doneButton;
	private StyledText vaultEntryCommentLabel;
	private Text vaultEntryNameInput;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_VaultEntrySetup inst = new DLG_VaultEntrySetup(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_VaultEntrySetup(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			
			
			dialogShell.setLayout(new FormLayout());

			{
				vaultEntryCommentInput = new StyledText(dialogShell, SWT.WRAP | SWT.V_SCROLL);
				FormData vaultEntryDescriptionEntryLData = new FormData();
				vaultEntryDescriptionEntryLData.width = 216;
				vaultEntryDescriptionEntryLData.height = 173;
				vaultEntryDescriptionEntryLData.left =  new FormAttachment(0, 1000, 12);
				vaultEntryDescriptionEntryLData.top =  new FormAttachment(0, 1000, 113);
				vaultEntryCommentInput.setLayoutData(vaultEntryDescriptionEntryLData);
				vaultEntryCommentInput.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));

			}
			{
				doneButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData doneButtonLData = new FormData();
				doneButtonLData.width = 40;
				doneButtonLData.height = 20;
				doneButtonLData.left =  new FormAttachment(0, 1000, 101);
				doneButtonLData.top =  new FormAttachment(0, 1000, 292);
				doneButton.setLayoutData(doneButtonLData);
				doneButton.setText("Done");
				doneButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						if (vaultEntryNameInput.getText().length() ==0) {
							MessageBox messageBox = new MessageBox(dialogShell, SWT.ICON_ERROR
							| SWT.OK);
							messageBox.setMessage("You need to provide a name for this campaign.");
							messageBox.open();
						} else {
							if(FileHandling.doesFolderExist(Globals.vaultfolderNAME+FileHandling.filenameCleaner(vaultEntryNameInput.getText()))) {
								Globals.mBox = new MessageBox(dialogShell.getShell(), SWT.ICON_INFORMATION|SWT.OK);
								Globals.mBox.setMessage("That name that is already in use. Please try again.");
								Globals.mBox.open();
							} else {
								// gather the information that will be used to create the new Campaign Info file
								Globals.campaignNameStorage = FileHandling.filenameCleaner(vaultEntryNameInput.getText());
								Globals.commentStorage = vaultEntryCommentInput.getText();
								dialogShell.dispose();
							}
							
						}
					}
				});
			}
			{
				vaultEntryCommentLabel = new StyledText(dialogShell, SWT.READ_ONLY | SWT.WRAP);
				FormData vaultEntryCommentLabelLData = new FormData();
				vaultEntryCommentLabelLData.width = 220;
				vaultEntryCommentLabelLData.height = 47;
				vaultEntryCommentLabelLData.left =  new FormAttachment(0, 1000, 12);
				vaultEntryCommentLabelLData.top =  new FormAttachment(0, 1000, 60);
				vaultEntryCommentLabel.setLayoutData(vaultEntryCommentLabelLData);
				vaultEntryCommentLabel.setText("Please provide a description for this vault entry.\n(Optional, but highly recommended.) ");
				vaultEntryCommentLabel.setBackground(SWTResourceManager.getColor(255,238,219));
				vaultEntryCommentLabel.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			{
				vaultEntryNameInput = new Text(dialogShell, SWT.NONE);
				FormData vaultEntryNameInputLData = new FormData();
				vaultEntryNameInputLData.width = 211;
				vaultEntryNameInputLData.height = 15;
				vaultEntryNameInputLData.left =  new FormAttachment(0, 1000, 12);
				vaultEntryNameInputLData.top =  new FormAttachment(0, 1000, 33);
				vaultEntryNameInput.setLayoutData(vaultEntryNameInputLData);
				vaultEntryNameInput.setTextLimit(30);
				vaultEntryNameInput.setFont(SWTResourceManager.getFont("Segoe UI", 9, 3, false, false));
			}
			{
				vaultEntryNameLabel = new Label(dialogShell, SWT.NONE);
				FormData vaultEntryNameLabelLData = new FormData();
				vaultEntryNameLabelLData.width = 228;
				vaultEntryNameLabelLData.height = 15;
				vaultEntryNameLabelLData.left =  new FormAttachment(0, 1000, 12);
				vaultEntryNameLabelLData.top =  new FormAttachment(0, 1000, 12);
				vaultEntryNameLabel.setLayoutData(vaultEntryNameLabelLData);
				vaultEntryNameLabel.setText("Select a name for this archived campaign");
				vaultEntryNameLabel.setBackground(SWTResourceManager.getColor(255,238,219));
				vaultEntryNameLabel.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 9, 0, false, false));
			}
			dialogShell.layout();
			dialogShell.pack();			
			dialogShell.setSize(268, 350);
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));
			dialogShell.setLocation(getParent().toDisplay(0,0));
			dialogShell.open();
			Display display = dialogShell.getDisplay();
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
				display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
