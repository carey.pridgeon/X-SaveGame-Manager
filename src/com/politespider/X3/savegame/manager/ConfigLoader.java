package com.politespider.X3.savegame.manager;

import java.io.FileInputStream;
import java.io.InputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.XMLEvent;

public class ConfigLoader {
	public static void load(String fName) {
		XMLEventReader eventReader = null;
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		InputStream in = null;
		try {
			in = new FileInputStream(fName);
		} catch (Exception e) {
			System.out.println("file not loaded");
		}
		try {
			eventReader = inputFactory.createXMLEventReader(in);
		} catch (Exception e) {	
			System.out.println("event reader error");
		}
		while (eventReader.hasNext()) {
			XMLEvent event = null;
			try {
				event = eventReader.nextEvent();
			} catch (Exception e) {
				System.out.println("eventReader.nextEvent error");
			}
			if (event.isStartElement()) {
				if (event.asStartElement().getName().getLocalPart() == ("saveFolderPath")) {
					try {
						event = eventReader.nextEvent();
					} catch (Exception e) {
						System.out.println("problem in long event");
					}
					Globals.specifiedSaveFolder = event.toString();
				}
			}
		}
	}
}
