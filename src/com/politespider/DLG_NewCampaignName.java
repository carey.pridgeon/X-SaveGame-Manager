package com.politespider;

import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import com.cloudgarden.resource.SWTResourceManager;

public class DLG_NewCampaignName extends org.eclipse.swt.widgets.Dialog {

	private Shell dialogShell;
	private Label newNameLabel;
	private Button doneButton;
	private Text newNameEntry;

	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Dialog inside a new Shell.
	*/
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell shell = new Shell(display);
			DLG_NewCampaignName inst = new DLG_NewCampaignName(shell, SWT.NULL);
			inst.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DLG_NewCampaignName(Shell parent, int style) {
		super(parent, style);
	}

	public void open() {
		try {
			Shell parent = getParent();
			dialogShell = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);

			{
				//Register as a resource user - SWTResourceManager will
				//handle the obtaining and disposing of resources
				SWTResourceManager.registerResourceUser(dialogShell);
			}
			
			
			dialogShell.setLayout(new FormLayout());
			dialogShell.layout();
			dialogShell.setSize(267, 114);
			dialogShell.setBackground(SWTResourceManager.getColor(255,238,219));
			{
				doneButton = new Button(dialogShell, SWT.PUSH | SWT.CENTER);
				FormData doneButtonLData = new FormData();
				doneButtonLData.left =  new FormAttachment(0, 1000, 93);
				doneButtonLData.top =  new FormAttachment(0, 1000, 47);
				doneButtonLData.width = 40;
				doneButtonLData.height = 25;
				doneButton.setLayoutData(doneButtonLData);
				doneButton.setText("Done");
				doneButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent evt) {
						if (newNameEntry.getText().length()!=0) {
							Globals.newCampaignName = newNameEntry.getText();
						}
						dialogShell.dispose();
					}
				});
			}
			{
				FormData newNameEntryLData = new FormData();
				newNameEntryLData.left =  new FormAttachment(0, 1000, 12);
				newNameEntryLData.top =  new FormAttachment(0, 1000, 26);
				newNameEntryLData.width = 221;
				newNameEntryLData.height = 15;
				newNameEntry = new Text(dialogShell, SWT.NONE);
				newNameEntry.setLayoutData(newNameEntryLData);
			}
			{
				newNameLabel = new Label(dialogShell, SWT.NONE);
				FormData newNameLabelLData = new FormData();
				newNameLabelLData.left =  new FormAttachment(0, 1000, 12);
				newNameLabelLData.top =  new FormAttachment(0, 1000, 5);
				newNameLabelLData.width = 203;
				newNameLabelLData.height = 15;
				newNameLabel.setLayoutData(newNameLabelLData);
				newNameLabel.setText("Enter the new name for this Campaign");
				newNameLabel.setBackground(SWTResourceManager.getColor(255, 238, 219));
			}
			dialogShell.pack();		
			dialogShell.setSize(255, 115);
			dialogShell.setLocation(getParent().toDisplay(100,0));
			dialogShell.open();
			Display display = dialogShell.getDisplay();
			while (!dialogShell.isDisposed()) {
				if (!display.readAndDispatch())
					display.sleep();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
